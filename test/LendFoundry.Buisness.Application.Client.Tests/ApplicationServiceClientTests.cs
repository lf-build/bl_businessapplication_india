﻿using LendFoundry.Business.Application.India;
using LendFoundry.Business.Application.India.Client;
using LendFoundry.Foundation.Client;
using Moq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;
using ApplicationObject = LendFoundry.Business.Application.India.Application;
namespace LendFoundry.Buisness.Application.Client.Tests
{
    public class ApplicationServiceClientTests
    {
        private ApplicationService ApplicationServiceClient { get; }
        private IRestRequest Request { get; set; }

        private Mock<IServiceClient> MockServiceClient { get; }
        public ApplicationServiceClientTests()
        {
            MockServiceClient = new Mock<IServiceClient>();
            ApplicationServiceClient = new ApplicationService(MockServiceClient.Object);
        }

       


        [Fact]
        public async void Client_GetByApplicationId()
        {

            MockServiceClient.Setup(s => s.ExecuteAsync<ApplicationObject>(It.IsAny<IRestRequest>()))
                 .Callback<IRestRequest>(r => Request = r)
                 .Returns(
                      () =>
                        Task.FromResult(
                            new ApplicationObject())); ;

            
            var result = await ApplicationServiceClient.GetByApplicationId("1");
            Assert.Equal("/{applicationNumber}", Request.Resource);
            Assert.Equal(Method.GET, Request.Method);
            Assert.NotNull(result);
        }

        [Fact]
        public async void Client_Add()
        {
            var application = new ApplicationRequest();

            MockServiceClient.Setup(s => s.ExecuteAsync<ApplicationResponse>(It.IsAny<IRestRequest>()))
                 .Callback<IRestRequest>(r => Request = r)
                 .Returns(
                      () =>
                        Task.FromResult(
                            new ApplicationResponse())); ;


            var result = await ApplicationServiceClient.Add(application);
            Assert.Equal("/", Request.Resource);
            Assert.Equal(Method.POST, Request.Method);
            Assert.NotNull(result);
        }

        [Fact]
        public async void Client_AddMerchantApplication()
        {
            var application = new ApplicationRequest();

            MockServiceClient.Setup(s => s.ExecuteAsync<ApplicationResponse>(It.IsAny<IRestRequest>()))
                 .Callback<IRestRequest>(r => Request = r)
                 .Returns(
                      () =>
                        Task.FromResult(
                            new ApplicationResponse())); ;


            var result = await ApplicationServiceClient.AddMerchantApplication(application);
            Assert.Equal("/lead", Request.Resource);
            Assert.Equal(Method.POST, Request.Method);
            Assert.NotNull(result);
        }

        [Fact]
        public async Task Client_LinkBankInformation_Application()
        {

            MockServiceClient.Setup(s => s.ExecuteAsync(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .Returns(
                     () =>
                       Task.FromResult(
                          true));

            await ApplicationServiceClient.LinkBankInformation("1", new BankInformation());
            Assert.Equal("/{applicationNumber}/bankinformation", Request.Resource);
            Assert.Equal(Method.PUT, Request.Method);
        }

        [Fact]
        public async Task Client_Get_ApplicationByApplicationNumber()
        {
            MockServiceClient.Setup(s => s.ExecuteAsync<ApplicationObject>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .Returns(
                     () =>
                       Task.FromResult(
                           new ApplicationObject()
                           { ApplicationNumber = "1" }));

            var result = await ApplicationServiceClient.GetByApplicationId("1");
            Assert.Equal("/{applicationNumber}", Request.Resource);
            Assert.Equal(Method.GET, Request.Method);
            Assert.NotNull(result);
        }

        [Fact]
        public async Task Client_LinkEmailInformation_Application()
        {

            MockServiceClient.Setup(s => s.ExecuteAsync(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .Returns(
                     () =>
                       Task.FromResult(
                          true));

            await ApplicationServiceClient.LinkEmailInformation("1", new EmailAddress());
            Assert.Equal("/{applicationNumber}/emailinforamtion", Request.Resource);
            Assert.Equal(Method.PUT, Request.Method);
        }

        [Fact]
        public async Task Client_LinkSetSelfDeclaredExpense_Application()
        {
            MockServiceClient.Setup(s => s.ExecuteAsync(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .Returns(
                     () =>
                       Task.FromResult(
                          true));

            await ApplicationServiceClient.SetSelfDeclaredInformation("1", new SelfDeclareInformation());
            Assert.Equal("/{applicationNumber}/declaredexpense", Request.Resource);
            Assert.Equal(Method.PUT, Request.Method);
        }

        [Fact]
        public async Task Client_SetOffers_Application()
        {
            MockServiceClient.Setup(s => s.ExecuteAsync(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .Returns(
                     () =>
                       Task.FromResult(
                          true));

            await ApplicationServiceClient.SetOffers("1", "initialoffer", new List<ILoanOffer>());
            Assert.Equal("{applicationNumber}/{offerType}/setoffers", Request.Resource);
            Assert.Equal(Method.PUT, Request.Method);
        }

        [Fact]
        public async Task Client_SetSelectedOffer_Application()
        {
            MockServiceClient.Setup(s => s.ExecuteAsync(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .Returns(
                     () =>
                       Task.FromResult(
                          true));

            await ApplicationServiceClient.SetSelectedOffer("1", "f01", "initialoffer");
            Assert.Equal("/{applicationNumber}/{offerId}/{offerType}/setselectedoffer", Request.Resource);
            Assert.Equal(Method.PUT, Request.Method);
        }

        [Fact]
        public async Task Client_getApplicationOffers_Application()
        {
            MockServiceClient.Setup(s => s.ExecuteAsync<List<ILoanOffer>>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r)
                .Returns(
                     () =>
                       Task.FromResult(
                            new List<ILoanOffer>()
                           ));

            await ApplicationServiceClient.GetApplicationOffers("1", "initialoffer");
            Assert.Equal("/{applicationNumber}/{offerType}/getoffers", Request.Resource);
            Assert.Equal(Method.GET, Request.Method);
        }


    }
}
