﻿using LendFoundry.Foundation.Date;
using LendFoundry.NumberGenerator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Business.Application.India.Tests.Fakes
{
    public class FakeApplicationService : IApplicationService
    {
        #region Private Properties

        private IGeneratorService ApplicationNumberGenerator { get; }
        private ApplicationConfiguration ApplicationConfigurations { get; }
        private ITenantTime TenantTime { get; set; }
        private List<IApplication> Applications { get; } = new List<IApplication>();
        private int CurrentId { get; set; }

        #endregion Private Properties

        #region Constructors

        public FakeApplicationService(ITenantTime tenantTime, IGeneratorService applicationNumberGenerator, ApplicationConfiguration applicationConfiguration, IEnumerable<IApplication> applications) : this(tenantTime)
        {
            Applications.AddRange(applications);
            CurrentId = Applications.Count + 1;
            ApplicationNumberGenerator = applicationNumberGenerator;
            ApplicationConfigurations = applicationConfiguration;
        }

        public FakeApplicationService(ITenantTime tenantTime)
        {
            TenantTime = tenantTime;
            CurrentId = 1;
        }

        #endregion Constructors

        public async Task<IApplicationResponse> Add(IApplicationRequest applicationRequest)
        {
            IApplication application = new Application(applicationRequest);
            application.ApplicationNumber = ApplicationNumberGenerator.TakeNext("application");

            application.ApplicationDate = TenantTime.Now;
            application.ExpiredDate = TenantTime.Now.AddDays(ApplicationConfigurations.ExpiryDays);

            return await Task.FromResult(new ApplicationResponse(application));
        }

        public async Task<IApplicationResponse> AddMerchantApplication(IApplicationRequest applicationRequest)
        {
            IApplication application = new Application(applicationRequest);
            application.ApplicationNumber = ApplicationNumberGenerator.TakeNext("application");

            application.ApplicationDate = TenantTime.Now;
            application.ExpiredDate = TenantTime.Now.AddDays(ApplicationConfigurations.ExpiryDays);

            return await Task.FromResult(new ApplicationResponse(application));
        }

        public Task<List<ILoanOffer>> GetApplicationOffers(string applicationNumber, string offerType)
        {
            throw new NotImplementedException();
        }

        public Task<IApplication> GetByApplicationId(string ApplicationNumber)
        {
            return Task.FromResult(Applications.Where(i => i.ApplicationNumber == ApplicationNumber).FirstOrDefault());
        }

        public Task<IApplication> GetByApplicationNumber(string ApplicationNumber)
        {
            return Task.FromResult(Applications.Where(i => i.ApplicationNumber == ApplicationNumber).FirstOrDefault());
        }

        public Task LinkBankInformation(string applicationId, IBankInformation bankInformationRequest)
        {
            throw new NotImplementedException();
        }

        public Task LinkEmailInformation(string applicationId, IEmailAddress emailInformationRequest)
        {
            throw new NotImplementedException();
        }

        public Task SetOffers(string applicationNumber, string offerType, IList<ILoanOffer> loanOfferRequest)
        {
            throw new NotImplementedException();
        }

        public Task SetSelectedOffer(string applicationNumber, string offerId, string offerType)
        {
            throw new NotImplementedException();
        }

        public Task SetSelfDeclaredInformation(string applicationId, ISelfDeclareInformation declareExpenseRequest)
        {
            throw new NotImplementedException();
        }

        public Task<IApplication> UpdateApplication(string applicationId, IApplicationUpdateRequest applicationRequest)
        {
            throw new NotImplementedException();
        }
    }
}