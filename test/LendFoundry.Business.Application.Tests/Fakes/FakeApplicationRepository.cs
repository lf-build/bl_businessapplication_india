﻿using LendFoundry.Business.Application.India.Persistence;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace LendFoundry.Business.Application.India.Tests.Fakes
{
    public class FakeApplicationRepository : IApplicationRepository
    {
        #region Constructors

        public FakeApplicationRepository(ITenantTime tenantTime, IEnumerable<IApplication> applications) : this(tenantTime)
        {
            Applications.AddRange(applications);
        }

        public FakeApplicationRepository(ITenantTime tenantTime)
        {
            TenantTime = tenantTime;
        }

        #endregion Constructors

        #region Private Properties

        public ITenantTime TenantTime { get; set; }
        public List<IApplication> Applications { get; set; } = new List<IApplication>();

        #endregion Private Properties

        #region Public Methods

        public Task<IApplication> Get(string id)
        {
            return Task.FromResult(Applications.FirstOrDefault(applicant => applicant.Id.Equals(id)));
        }

        public void Add(IApplication application)
        {
            //IT IS JUST FOR ROLLBACK TEST
            if (application.PurposeOfLoan == PurposeOfLoan.Vacation)
                throw new ArgumentException(nameof(application));
            Applications.Add(application);
        }

        public void Remove(IApplication application)
        {
            Applications.Remove(application);
        }

        public void Update(IApplication application)
        {
            throw new NotImplementedException();
        }

        public async Task<IApplication> GetByApplicationId(string applicationNumber)
        {
            return await Task.Run(() =>
            {
                var applicationData = Applications.Where(application => application.ApplicationNumber == applicationNumber).FirstOrDefault();

                if (applicationData == null)
                    throw new NotFoundException($"Application {applicationNumber} not found");

                return applicationData;
            });
        }

        public Task<IApplication> UpdateApplication(string applicationNumber, IApplicationUpdateRequest applicationUpdateRequest)
        {
            throw new NotImplementedException();
        }

        public async Task<IApplication> LinkBankInformation(string applicationNumber, IBankInformation bankInformation)
        {
            return await Task.Run(() =>
            {
                var applicationData = Applications.Where(application => application.ApplicationNumber == applicationNumber).FirstOrDefault();

                if (applicationData == null)
                    throw new NotFoundException($"Application {applicationNumber} not found");

                applicationData.LinkedBankInformation = bankInformation;
                return applicationData;
            });
        }

        public async Task<IApplication> LinkEmailInformation(string applicationNumber, IEmailAddress emailInfo)
        {
            return await Task.Run(() =>
            {
                var applicationData = Applications.Where(application => application.ApplicationNumber == applicationNumber).FirstOrDefault();

                if (applicationData == null)
                    throw new NotFoundException($"Application {applicationNumber} not found");

                applicationData.LinkedEmailAddress = emailInfo;
                return applicationData;
            });
        }

        public Task<IApplication> UpdateSelfDeclaredExpense(string applicationId, SelfDeclareInformation declareExpense)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<IApplication>> All(Expression<Func<IApplication, bool>> query, int? skip = default(int?), int? quantity = default(int?))
        {
            throw new NotImplementedException();
        }

        public int Count(Expression<Func<IApplication, bool>> query)
        {
            throw new NotImplementedException();
        }

        public string GetApplicantId(string applicationId)
        {
            throw new NotImplementedException();
        }

        public async Task<IApplication> SetSelfDeclaredInformation(string applicationNumber, ISelfDeclareInformation expenseInformation)
        {
            return await Task.Run(() =>
            {
                var applicationData = Applications.Where(application => application.ApplicationNumber == applicationNumber).FirstOrDefault();

                if (applicationData == null)
                    throw new NotFoundException($"Application {applicationNumber} not found");

                applicationData.SelfDeclareInformation = expenseInformation;
                return applicationData;
            });
        }

        public async Task<IApplication> SetInitialOffers(string applicationNumber, IList<ILoanOffer> initialLoanOffers)
        {
            return await Task.Run(() =>
            {
                var applicationData = Applications.Where(application => application.ApplicationNumber == applicationNumber).FirstOrDefault();

                if (applicationData == null)
                    throw new NotFoundException($"Application {applicationNumber} not found");

                applicationData.InitialOffers = initialLoanOffers;
                return applicationData;
            });
        }

        public async Task<IApplication> SetFinalOffers(string applicationNumber, IList<ILoanOffer> finalLoanOffers)
        {
            return await Task.Run(() =>
            {
                var applicationData = Applications.Where(application => application.ApplicationNumber == applicationNumber).FirstOrDefault();

                if (applicationData == null)
                    throw new NotFoundException($"Application {applicationNumber} not found");

                applicationData.FinalOffers = finalLoanOffers;
                return applicationData;
            });
        }

        public async Task<IApplication> SetSelectedInitialOffer(string applicationNumber, string offerId, string offerType)
        {
            return await Task.Run(() =>
            {
                var applicationData = Applications.Where(application => application.ApplicationNumber == applicationNumber).FirstOrDefault();

                if (applicationData == null)
                    throw new NotFoundException($"Application {applicationNumber} not found");

                if (applicationData.InitialOffers == null)
                    throw new NotFoundException($"Initial offers for {applicationNumber} not found");

                var lstInitialOffers = applicationData.InitialOffers;

                var currentSelectedOffer = lstInitialOffers.FirstOrDefault(offer => offer.IsSelectedOffer == true);
                if (currentSelectedOffer != null)
                {
                    currentSelectedOffer.IsSelectedOffer = false;
                }

                var currentOffer = lstInitialOffers.FirstOrDefault(offer => offer.OfferId.Equals(offerId));
                if (currentOffer == null)
                    throw new NotFoundException($"Initial offer with id { offerId } is not found");
                currentOffer.IsSelectedOffer = true;

                applicationData.InitialOffers = lstInitialOffers;

                return applicationData;
            });
        }

        public async Task<IApplication> SetSelectedFinalOffer(string applicationNumber, string offerId, string offerType)
        {
            return await Task.Run(() =>
            {
                var applicationData = Applications.Where(application => application.ApplicationNumber == applicationNumber).FirstOrDefault();

                if (applicationData == null)
                    throw new NotFoundException($"Application {applicationNumber} not found");

                if (applicationData.FinalOffers == null)
                    throw new NotFoundException($"Initial offers for {applicationNumber} not found");

                var lstFinalOffers = applicationData.FinalOffers;

                var currentSelectedOffer = lstFinalOffers.FirstOrDefault(offer => offer.IsSelectedOffer == true);
                if (currentSelectedOffer != null)
                {
                    currentSelectedOffer.IsSelectedOffer = false;
                }

                var currentOffer = lstFinalOffers.FirstOrDefault(offer => offer.OfferId.Equals(offerId));
                if (currentOffer == null)
                    throw new NotFoundException($"Initial offer with id { offerId } is not found");
                currentOffer.IsSelectedOffer = true;

                applicationData.FinalOffers = lstFinalOffers;

                return applicationData;
            });
        }

        public async Task<List<ILoanOffer>> GetInitalOffers(string applicationNumber)
        {
            return await Task.Run(() =>
            {
                var applicationData = Applications.Where(application => application.ApplicationNumber == applicationNumber).FirstOrDefault();

                if (applicationData == null)
                    throw new NotFoundException($"Application {applicationNumber} not found");

                if (applicationData.InitialOffers == null || applicationData.InitialOffers.ToList().Count <= 0)
                    throw new NotFoundException($"Initial offers for {applicationNumber} not found");

                return applicationData.InitialOffers.ToList();
            });
        }

        public async Task<List<ILoanOffer>> GetFinalOffers(string applicationNumber)
        {
            return await Task.Run(() =>
            {
                var applicationData = Applications.Where(application => application.ApplicationNumber == applicationNumber).FirstOrDefault();

                if (applicationData == null)
                    throw new NotFoundException($"Application {applicationNumber} not found");

                if (applicationData.FinalOffers == null || applicationData.FinalOffers.ToList().Count <= 0)
                    throw new NotFoundException($"Initial offers for {applicationNumber} not found");

                return applicationData.FinalOffers.ToList();
            });
        }

        #endregion Public Methods
    }
}