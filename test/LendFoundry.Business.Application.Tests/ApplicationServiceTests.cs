﻿using LendFoundry.Business.Applicant.India;
using LendFoundry.Business.Application.India.Persistence;
using LendFoundry.Business.Application.India.Tests.Fakes;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Client;
using Moq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace LendFoundry.Business.Application.India.Tests
{
    public class ApplicationServiceTests
    {
        #region Public Methods
       
        [Fact]
        public async Task ApplicationService_Add_ThrowException_When_Values_Are_Null()
        {
            var fakeApplicantService = GetApplicantService();
            var fakeApplicationRepository = GetFakeApplicationRepository();
            var service = GetApplicationService(fakeApplicantService, fakeApplicationRepository);
            var applicationRequest = new ApplicationRequest();

            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(null));
            applicationRequest.RequestedAmount = -10;
            // When RequestedAmount is less than or equal to 0
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(applicationRequest));

            applicationRequest.RequestedAmount = 10;
            // When Purpose of Loan is not defined
            await Assert.ThrowsAsync<InvalidEnumArgumentException>(async () => await service.Add(applicationRequest));

            applicationRequest.PurposeOfLoan = PurposeOfLoan.CarFinancing;
            //When Source is null
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(applicationRequest));

            applicationRequest.Source = new Source()
            {

            };
            //When Source ReferenceId is null
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(applicationRequest));

            applicationRequest.Source = new Source()
            {
                TrackingCode = "Code123",
                SourceReferenceId = "1452"
               
                
            };

            // When Source Type is not defined
            await Assert.ThrowsAsync<InvalidEnumArgumentException>(async () => await service.Add(applicationRequest));

            applicationRequest.Source = new Source()
            {
                SourceType = SourceType.Merchant,

                TrackingCode = "Code123",
                SourceReferenceId = "Refe111"
            };
            // When Primary Email is not defined 
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(applicationRequest));

            applicationRequest.PrimaryEmail = new LendFoundry.Business.Application.India.EmailAddress();
            //PrimaryPhone
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(applicationRequest));
            applicationRequest.PrimaryPhone = new LendFoundry.Business.Application.India.PhoneNumber();

            //PrimaryApplicant
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(applicationRequest));

            applicationRequest.PrimaryApplicant = new LendFoundry.Business.Applicant.India.ApplicantRequest();

            // Primary Fax
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(applicationRequest));
            applicationRequest.PrimaryFax = "testFax";

            // Primary Address
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(applicationRequest));
            applicationRequest.PrimaryAddress = new LendFoundry.Business.Application.India.Address();

            // PrimaryApplicant UserId -UserName Null
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(applicationRequest));
            applicationRequest.PrimaryApplicant.UserName = "testUserName";

            //Password Null
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(applicationRequest));
            applicationRequest.PrimaryApplicant.Password = "testPassword";

            //OwnerNull
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(applicationRequest));
            applicationRequest.PrimaryApplicant.Owners = new List<IOwner>();

            Owner owner = new Owner();
            applicationRequest.PrimaryApplicant.Owners.Add(owner);
            //Owner -First Name 
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(applicationRequest));

            applicationRequest.PrimaryApplicant.Owners.FirstOrDefault().FirstName = "Test";
            //Owner -SSN
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(applicationRequest));

            applicationRequest.PrimaryApplicant.Owners.FirstOrDefault().SSN = "Test";
            //Owner - DOB
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(applicationRequest));

            applicationRequest.PrimaryApplicant.Owners.FirstOrDefault().DOB = "date";
            // OwnerShip
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(applicationRequest));
            applicationRequest.PrimaryApplicant.Owners.FirstOrDefault().Ownership = "OwnerShip";

            // Designation
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(applicationRequest));
            applicationRequest.PrimaryApplicant.Owners.FirstOrDefault().Designation = "Designation";

            // PrimaryApplicant Address null
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(applicationRequest));
            applicationRequest.PrimaryApplicant.Addresses = new List<LendFoundry.Business.Applicant.India.IAddress>();

            LendFoundry.Business.Applicant.India.Address address = new LendFoundry.Business.Applicant.India.Address();
            applicationRequest.PrimaryApplicant.Addresses.Add(address);
            // PrimaryApplicant AddressLine 1
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(applicationRequest));
            applicationRequest.PrimaryApplicant.Addresses.FirstOrDefault().AddressLine1 = "Test";

            // PrimaryApplicant City
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(applicationRequest));
            applicationRequest.PrimaryApplicant.Addresses.FirstOrDefault().City = "Test";

            // PrimaryApplicant State
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(applicationRequest));
            applicationRequest.PrimaryApplicant.Addresses.FirstOrDefault().State = "Test";


            // PrimaryApplicant PinCode
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(applicationRequest));
            applicationRequest.PrimaryApplicant.Addresses.FirstOrDefault().PinCode = "Test";


            // PrimaryApplicant Country
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(applicationRequest));
            applicationRequest.PrimaryApplicant.Addresses.FirstOrDefault().Country = "Test";

            // AddressType
            applicationRequest.PrimaryApplicant.Addresses.FirstOrDefault().AddressType = Applicant.AddressType.Home;


            // PrimaryApplicant EmailAddress - Null
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(applicationRequest));
            applicationRequest.PrimaryApplicant.EmailAddresses = new List<LendFoundry.Business.Applicant.India.IEmailAddress>();

            LendFoundry.Business.Applicant.India.EmailAddress emailAddress = new LendFoundry.Business.Applicant.India.EmailAddress();
            applicationRequest.PrimaryApplicant.EmailAddresses.Add(emailAddress);

            // PrimaryApplicant EmailAddress - Email Null
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(applicationRequest));
            applicationRequest.PrimaryApplicant.EmailAddresses.FirstOrDefault().Email = "test";

            // PrimaryApplicant EmailAddress - EmailType
            await Assert.ThrowsAsync<InvalidEnumArgumentException>(async () => await service.Add(applicationRequest));
            applicationRequest.PrimaryApplicant.EmailAddresses.FirstOrDefault().EmailType = Applicant.EmailType.Personal;


        }

        [Fact]
        public async Task ApplicationService_Add()
        {
            var fakeApplicantService = GetApplicantService();
            var fakeApplicationRepository = GetFakeApplicationRepository();
            var service = GetApplicationService(fakeApplicantService, fakeApplicationRepository);

            var addedApplication = await service.Add(new ApplicationRequest()
            {
                PrimaryApplicant = new ApplicantRequest()
                {
                    PrimaryAddress = new LendFoundry.Business.Applicant.India.Address(),
                    PrimaryEmail = new LendFoundry.Business.Applicant.India.EmailAddress(),
                    PrimaryFax = "test",
                    PrimaryPhone = new LendFoundry.Business.Applicant.India.PhoneNumber(),
                    Addresses = new List<Applicant.IAddress>
                    {
                        new Applicant.Address()
                        {
                            AddressLine1 = "test1",
                            AddressLine2 = "test3",
                            City = "Ahmedabad",
                            Country = "India",
                            PinCode= "380034",
                            State = "Gujarat",
                            AddressType = Applicant.AddressType.Business
                        }
                    },

                    EmailAddresses = new List<Applicant.IEmailAddress>
                    {
                        new Applicant.EmailAddress()
                        {
                            Email = "test@gmail.com",
                            EmailType = Applicant.EmailType.Personal,
                            IsDefault = true,
                            IsVerified = false
                        }
                    },

                    UserName = "TestUser111",
                    Password = "TestPassword",
                    Owners = new List<Applicant.IOwner>
                     {
                         new Owner
                         {
                              DOB = new DateTimeOffset().ToString(),
                                FirstName = "Test First Name",
                                LastName = "Test Last Name",
                                SSN = "Test",
                                 Ownership = "TT",
                                  Designation = "Mr",
                                   OwnershipType = OwnershipType.OwnershipType1
                         }
                     },


                },
                PurposeOfLoan = PurposeOfLoan.DebtConsolidation,
                Source = new Source()
                {
                    SourceType = SourceType.Merchant,
                    TrackingCode = "Code123",
                    SourceReferenceId = "Refe123"
                },

                RequestedAmount = 10000,
                RequestedTermType = LoanFrequency.Monthly,
                RequestedTermValue = 12.4333,
                PrimaryEmail = new EmailAddress(),
                PrimaryPhone = new PhoneNumber(),
                PrimaryFax = "testFax",
                PrimaryAddress = new Address(),
                SelfDeclareInformation = new SelfDeclareInformation()
            });
            Assert.Equal("1", addedApplication.ApplicationNumber);
            // Assert.Equal("200.02", addedApplication.ApplicationStatus);
            Assert.Equal(addedApplication.ApplicationDate.AddDays(60), addedApplication.ExpiryDate);
        }

        [Fact]
        public async Task ApplicationService_AddMerchantApplication()
        {
            var fakeApplicantService = GetApplicantService();
            var fakeApplicationRepository = GetFakeApplicationRepository();
            var service = GetApplicationService(fakeApplicantService, fakeApplicationRepository);

            var addedApplication = await service.AddMerchantApplication(new ApplicationRequest()
            {
                PrimaryApplicant = new ApplicantRequest()
                {

                    Addresses = new List<Applicant.IAddress>
                    {
                        new Applicant.Address()
                        {
                            AddressLine1 = "test1",
                            AddressLine2 = "test3",
                            City = "Ahmedabad",
                            Country = "India",
                            PinCode= "380034",
                            State = "Gujarat",
                            AddressType = Applicant.AddressType.Business
                        }
                    },

                    EmailAddresses = new List<Applicant.IEmailAddress>
                    {
                        new Applicant.EmailAddress()
                        {
                            Email = "test@gmail.com",
                            EmailType = Applicant.EmailType.Personal,
                            IsDefault = true,
                            IsVerified = false
                        }
                    },

                    UserName = "TestUser111",
                    Password = "TestPassword",
                },
                Source = new Source()
                {
                    SourceType = SourceType.Merchant,
                    TrackingCode = "Code123",
                    SourceReferenceId = "Refe123"
                },

                RequestedAmount = 10000,
                RequestedTermType = LoanFrequency.Monthly,
                RequestedTermValue = 12.4333
            });
            Assert.Equal("1", addedApplication.ApplicationNumber);
            //Assert.Equal("200.02", addedApplication);
            Assert.Equal(addedApplication.ApplicationDate.AddDays(60), addedApplication.ExpiryDate);
        }

        [Fact]
        public async void ApplicationService_LinkBankInformation_Success()
        {
            var application = new List<IApplication>
            {

                new Application()
            {

                Source = new Source()
                {
                    SourceType = SourceType.Merchant,
                    TrackingCode = "Code123",
                    SourceReferenceId = "Refe123"
                },

                RequestedAmount = 10000,
                RequestedTermType = LoanFrequency.Monthly,
                RequestedTermValue = 12.4333,
                ApplicationNumber = "1",
                ApplicationStatus = "200.02",
                LinkedBankInformation = new BankInformation()
                {
                        BankName = "HDFC",
                        AccountNumber = "567907845",
                        AccountHolderName = "SigmaInfo",
                        RoutingNumber = "ifsc1234",
                        AccountType =  AccountType.Savings
                }
            }};

            var fakeApplicantService = GetApplicantService();
            var fakeApplicationRepository = GetFakeApplicationRepository(application);
            var service = GetApplicationService(fakeApplicantService, fakeApplicationRepository);

            var bankInfo = new BankInformation()
            {
                BankName = "Axis",
                AccountNumber = "1223344556",
                AccountHolderName = "SIS",
                RoutingNumber = "Ifsc001",
                AccountType = AccountType.Savings
            };

            await service.LinkBankInformation("1", bankInfo);
            Assert.NotNull(fakeApplicationRepository.Applications);
            Assert.Equal(AccountType.Savings, fakeApplicationRepository.Applications.ToList().First().LinkedBankInformation.AccountType);
        }


        [Fact]
        public async void ApplicationService_LinkBankInformation_ThrowsWhenInputsAreInvalid()
        {
            var application = new List<IApplication>
            {

                new Application()
            {

                Source = new Source()
                {
                    SourceType = SourceType.Merchant,
                    TrackingCode = "Code123",
                    SourceReferenceId = "Refe123"
                },

                RequestedAmount = 10000,
                RequestedTermType = LoanFrequency.Monthly,
                RequestedTermValue = 12.4333,
                ApplicantId = "1",
                ApplicationStatus = "200.02",
                LinkedBankInformation = new BankInformation()
                {
                        BankName = "HDFC",
                        AccountNumber = "567907845",
                        AccountHolderName = "SigmaInfo",
                        RoutingNumber = "ifsc1234",
                        AccountType =  AccountType.Savings
                }
            }};

            var fakeApplicantService = GetApplicantService();
            var fakeApplicationRepository = GetFakeApplicationRepository(application);
            var service = GetApplicationService(fakeApplicantService, fakeApplicationRepository);

            var bankInfo = new BankInformation()
            {
                BankName = "",
                AccountNumber = "",
                AccountHolderName = "",
                RoutingNumber = ""
            };

            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.LinkBankInformation(null, bankInfo));
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.LinkBankInformation("1", null));
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.LinkBankInformation("1", bankInfo));
        }

        [Fact]
        public async void ApplicationService_LinkEmailAddress_Success()
        {
            var application = new List<IApplication>
            {

                new Application()
            {

                Source = new Source()
                {
                    SourceType = SourceType.Merchant,
                    TrackingCode = "Code123",
                    SourceReferenceId = "Refe123"
                },

                RequestedAmount = 10000,
                RequestedTermType = LoanFrequency.Monthly,
                RequestedTermValue = 12.4333,
                ApplicationNumber = "1",
                ApplicationStatus = "200.02",
                LinkedBankInformation = new BankInformation()
                {
                        BankName = "HDFC",
                        AccountNumber = "567907845",
                        AccountHolderName = "SigmaInfo",
                        RoutingNumber = "ifsc1234",
                        AccountType =  AccountType.Savings
                },
                LinkedEmailAddress = new EmailAddress()
                {
                    Email = "myemail@sigmainfo.net",
                    IsDefault = true,
                    IsVerified = false,
                    EmailType = EmailType.Work
                }
            }};

            var fakeApplicantService = GetApplicantService();
            var fakeApplicationRepository = GetFakeApplicationRepository(application);
            var service = GetApplicationService(fakeApplicantService, fakeApplicationRepository);

            var emailInfo = new EmailAddress()
            {
                Email = "testemail@sigmainfo.net",
                IsDefault = false,
                IsVerified = false,
                EmailType = EmailType.Personal
            };

            await service.LinkEmailInformation("1", emailInfo);
            Assert.NotNull(fakeApplicationRepository.Applications);
            Assert.Equal(EmailType.Personal, fakeApplicationRepository.Applications.ToList().First().LinkedEmailAddress.EmailType);
            Assert.Equal("testemail@sigmainfo.net", fakeApplicationRepository.Applications.ToList().First().LinkedEmailAddress.Email);
            Assert.Equal(false, fakeApplicationRepository.Applications.ToList().First().LinkedEmailAddress.IsDefault);
        }

        [Fact]
        public async void ApplicationService_LinkEmailInformation_ThrowsWhenInputsAreInvalid()
        {
            var application = new List<IApplication>
            {

                new Application()
            {

                Source = new Source()
                {
                    SourceType = SourceType.Merchant,
                    TrackingCode = "Code123",
                    SourceReferenceId = "Refe123"
                },

                RequestedAmount = 10000,
                RequestedTermType = LoanFrequency.Monthly,
                RequestedTermValue = 12.4333,
                ApplicationNumber = "1",
                ApplicationStatus = "200.02",
                LinkedBankInformation = new BankInformation()
                {
                        BankName = "HDFC",
                        AccountNumber = "567907845",
                        AccountHolderName = "SigmaInfo",
                        RoutingNumber = "ifsc1234",
                        AccountType =  AccountType.Savings
                },
                LinkedEmailAddress = new EmailAddress()
                {
                    Email = "myemail@sigmainfo.net",
                    IsDefault = true,
                    IsVerified = false,
                    EmailType = EmailType.Work
                }
            }};

            var fakeApplicantService = GetApplicantService();
            var fakeApplicationRepository = GetFakeApplicationRepository(application);
            var service = GetApplicationService(fakeApplicantService, fakeApplicationRepository);

            var emailInfo = new EmailAddress()
            {
                Email = "",
                IsDefault = false,
                IsVerified = false,
                EmailType = EmailType.Personal
            };

            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.LinkEmailInformation(null, emailInfo));
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.LinkEmailInformation("1", null));
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.LinkEmailInformation("1", emailInfo));
        }


        [Fact]
        public async void ApplicationService_GetByApplicationNumber_Should_Throw_Exception()
        {
            List<IApplication> application = GetApplicationData();

            var fakeApplicantService = GetApplicantService();
            var fakeApplicationRepository = GetFakeApplicationRepository(application);
            var service = GetApplicationService(fakeApplicantService, fakeApplicationRepository);



            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.GetByApplicationId(null));
            await Assert.ThrowsAsync<NotFoundException>(async () => await service.GetByApplicationId("2"));
          
        }

        [Fact]
        public async void ApplicationService_GetApplicationOffers_Should_Throw_Exception()
        {
            List<IApplication> application = GetApplicationData();

            var fakeApplicantService = GetApplicantService();
            var fakeApplicationRepository = GetFakeApplicationRepository(application);
            var service = GetApplicationService(fakeApplicantService, fakeApplicationRepository);



            await Assert.ThrowsAsync<ArgumentException>(async () => await service.GetApplicationOffers(null,null));

            await Assert.ThrowsAsync<ArgumentException>(async () => await service.GetApplicationOffers("1",null));

            await Assert.ThrowsAsync<InvalidArgumentException>(async () => await service.GetApplicationOffers("1", "initialoffer1"));
        }

        [Fact]
        public async void ApplicationService_GetApplicationOffers_For_Final_Offer()
        {
            List<IApplication> application = GetApplicationData();

            var fakeApplicantService = GetApplicantService();
            var fakeApplicationRepository = GetFakeApplicationRepository(application);
            var service = GetApplicationService(fakeApplicantService, fakeApplicationRepository);

            await service.GetApplicationOffers("1", "finaloffer");

            Assert.NotNull(service.GetApplicationOffers("1", "finaloffer"));

        }

        [Fact]
        public async void ApplicationService_LinkSelfDeclaredExpense_Success()
        {
            List<IApplication> application = GetApplicationData();

            var fakeApplicantService = GetApplicantService();
            var fakeApplicationRepository = GetFakeApplicationRepository(application);
            var service = GetApplicationService(fakeApplicantService, fakeApplicationRepository);

            var expense = new SelfDeclareInformation()
            {
                AnnualRevenue = 1000,
                SelfReportedAnnualRevenue = 500000,
                AverageBankBalance = 2000,
                IsExistingBusinessLoan = true
            };

            await service.SetSelfDeclaredInformation("1", expense);
            Assert.NotNull(fakeApplicationRepository.Applications);
            Assert.Equal(1000, fakeApplicationRepository.Applications.ToList().First().SelfDeclareInformation.AnnualRevenue);
            Assert.Equal(500000, fakeApplicationRepository.Applications.ToList().First().SelfDeclareInformation.SelfReportedAnnualRevenue);
            Assert.Equal(2000, fakeApplicationRepository.Applications.ToList().First().SelfDeclareInformation.AverageBankBalance);

        }

     

        [Fact]
        public async void ApplicationService_LinkSelfDeclaredExpense_ThrowsWhenInputsAreInvalid()
        {
            List<IApplication> application = GetApplicationData();

            var fakeApplicantService = GetApplicantService();
            var fakeApplicationRepository = GetFakeApplicationRepository(application);
            var service = GetApplicationService(fakeApplicantService, fakeApplicationRepository);

            var expense = new SelfDeclareInformation()
            {
                AnnualRevenue = 1000,
                SelfReportedAnnualRevenue = 500000,
                AverageBankBalance = 2000,
                IsExistingBusinessLoan = true
            };

            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.SetSelfDeclaredInformation(null, expense));
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.SetSelfDeclaredInformation("1", null));
        }

        [Fact]
        public async void ApplicationService_SetInitialOffer_Success()
        {
            List<IApplication> application = GetApplicationData();

            var fakeApplicantService = GetApplicantService();
            var fakeApplicationRepository = GetFakeApplicationRepository(application);
            var service = GetApplicationService(fakeApplicantService, fakeApplicationRepository);

            var initialOffers = new List<ILoanOffer>()
            {
                 new LoanOffer()
                    {
                        AnnualPercentageRate = 15.90,
                        DisbursedLoanAmount = 199000,
                        GivenLoanAmount = 200000,
                        InterestRate = 10,
                        IsSelectedOffer = false,
                        NumberOfIntallments = 60,
                        OfferFees = new List<IOfferFee>
                        {
                            new OfferFee()
                             {
                                FeeAmount = 10000,
                                FeePercentage = 5,
                                FeeType = OfferFeeType.OneTime,
                                IsIncludedInLoanAmount = true
                            }
                        },
                        RepaymentFrequency = LoanFrequency.Monthly,
                        RepaymentInstallmentAmount = 15000
                    },
                  new LoanOffer()
                    {
                        AnnualPercentageRate = 10.23,
                        DisbursedLoanAmount = 199000,
                        GivenLoanAmount = 200000,
                        InterestRate = 12,
                        IsSelectedOffer = false,
                        NumberOfIntallments = 36,
                        OfferFees = new List<IOfferFee>
                        {
                            new OfferFee()
                             {
                                FeeAmount = 10000,
                                FeePercentage = 5,
                                FeeType = OfferFeeType.OneTime,
                                IsIncludedInLoanAmount = true
                            }
                        },
                        RepaymentFrequency = LoanFrequency.Monthly,
                        RepaymentInstallmentAmount = 25000
                    }
            };

            await service.SetOffers("1", "initialoffer", initialOffers);
            Assert.Equal(2, fakeApplicationRepository.Applications.First().InitialOffers.Count);
            Assert.NotNull(fakeApplicationRepository.Applications.First().InitialOffers.First().OfferId);
        
        }

        [Fact]
        public async void ApplicationService_SetInitialOffer_ThrowsExceoptionOnInvalidData()
        {
            List<IApplication> application = GetApplicationData();

            var fakeApplicantService = GetApplicantService();
            var fakeApplicationRepository = GetFakeApplicationRepository(application);
            var service = GetApplicationService(fakeApplicantService, fakeApplicationRepository);

            var initialOffers = new List<ILoanOffer>()
            {
                 new LoanOffer()
                    {
                        AnnualPercentageRate = 0,
                        DisbursedLoanAmount = 0,
                        GivenLoanAmount = 0,
                        InterestRate = 0,
                        IsSelectedOffer = false,
                        NumberOfIntallments = 0,
                        OfferFees = new List<IOfferFee>
                        {
                            new OfferFee()
                             {
                                FeeAmount = 10000,
                                FeePercentage = 5,
                                FeeType = OfferFeeType.OneTime,
                                IsIncludedInLoanAmount = true
                            }
                        },
                        RepaymentFrequency = LoanFrequency.Monthly,
                        RepaymentInstallmentAmount = 0
                    }
               
            };

            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.SetOffers(null, "initialOffer", initialOffers));
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.SetOffers("1", "", initialOffers));
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.SetOffers("1", "initialOffer", null));
            await Assert.ThrowsAsync<ArgumentException>(async () => await service.SetOffers("1", "initialOffer", initialOffers));
            initialOffers.FirstOrDefault().AnnualPercentageRate = 10;

            //Given Loan Amount 
            await Assert.ThrowsAsync<ArgumentException>(async () => await service.SetOffers("1", "initialOffer", initialOffers));
            initialOffers.FirstOrDefault().GivenLoanAmount = 200;
            await Assert.ThrowsAsync<ArgumentException>(async () => await service.SetOffers("1", "initialOffer", initialOffers));

            initialOffers.FirstOrDefault().InterestRate = 200;
            await Assert.ThrowsAsync<ArgumentException>(async () => await service.SetOffers("1", "initialOffer", initialOffers));

            initialOffers.FirstOrDefault().DisbursedLoanAmount = 200;
            await Assert.ThrowsAsync<ArgumentException>(async () => await service.SetOffers("1", "initialOffer", initialOffers));

            initialOffers.FirstOrDefault().NumberOfIntallments = 200;
            
        }

        [Fact]
        public async void ApplicationService_SetFinalOffer_Success()
        {
            List<IApplication> application = GetApplicationData();

            var fakeApplicantService = GetApplicantService();
            var fakeApplicationRepository = GetFakeApplicationRepository(application);
            var service = GetApplicationService(fakeApplicantService, fakeApplicationRepository);

            var finalOffers = new List<ILoanOffer>()
            {
                 new LoanOffer()
                    {
                        AnnualPercentageRate = 15.90,
                        DisbursedLoanAmount = 199000,
                        GivenLoanAmount = 200000,
                        InterestRate = 10,
                        IsSelectedOffer = false,
                        NumberOfIntallments = 60,
                        OfferFees = new List<IOfferFee>
                        {
                            new OfferFee()
                             {
                                FeeAmount = 10000,
                                FeePercentage = 5,
                                FeeType = OfferFeeType.OneTime,
                                IsIncludedInLoanAmount = true
                            }
                        },
                        RepaymentFrequency = LoanFrequency.Monthly,
                        RepaymentInstallmentAmount = 15000
                    },
                  new LoanOffer()
                    {
                        AnnualPercentageRate = 10.23,
                        DisbursedLoanAmount = 199000,
                        GivenLoanAmount = 200000,
                        InterestRate = 12,
                        IsSelectedOffer = false,
                        NumberOfIntallments = 36,
                        OfferFees = new List<IOfferFee>
                        {
                            new OfferFee()
                             {
                                FeeAmount = 10000,
                                FeePercentage = 5,
                                FeeType = OfferFeeType.OneTime,
                                IsIncludedInLoanAmount = true
                            }
                        },
                        RepaymentFrequency = LoanFrequency.Monthly,
                        RepaymentInstallmentAmount = 25000
                    }
            };

            await service.SetOffers("1", "finaloffer", finalOffers);
            Assert.Equal(2, fakeApplicationRepository.Applications.First().FinalOffers.Count);
            Assert.NotNull(fakeApplicationRepository.Applications.First().FinalOffers.First().OfferId);
            Assert.NotNull(fakeApplicationRepository.Applications.First().FinalOffers[1].OfferId);
           
        }


        [Fact]
        public async void SetSelectedOffer_Throw_Exception_When_OfferType_Is_Not_Valid()
        {
            List<IApplication> application = GetApplicationData();

            var fakeApplicantService = GetApplicantService();
            var fakeApplicationRepository = GetFakeApplicationRepository(application);
            var service = GetApplicationService(fakeApplicantService, fakeApplicationRepository);

            var finalOffers = new List<ILoanOffer>()
            {
                 new LoanOffer()
                    {
                        AnnualPercentageRate = 15.90,
                        DisbursedLoanAmount = 199000,
                        GivenLoanAmount = 200000,
                        InterestRate = 10,
                        IsSelectedOffer = false,
                        NumberOfIntallments = 60,
                        OfferFees = new List<IOfferFee>
                        {
                            new OfferFee()
                             {
                                FeeAmount = 10000,
                                FeePercentage = 5,
                                FeeType = OfferFeeType.OneTime,
                                IsIncludedInLoanAmount = true
                            }
                        },
                        RepaymentFrequency = LoanFrequency.Monthly,
                        RepaymentInstallmentAmount = 15000
                    },
                  new LoanOffer()
                    {
                        AnnualPercentageRate = 10.23,
                        DisbursedLoanAmount = 199000,
                        GivenLoanAmount = 200000,
                        InterestRate = 12,
                        IsSelectedOffer = false,
                        NumberOfIntallments = 36,
                        OfferFees = new List<IOfferFee>
                        {
                            new OfferFee()
                             {
                                FeeAmount = 10000,
                                FeePercentage = 5,
                                FeeType = OfferFeeType.OneTime,
                                IsIncludedInLoanAmount = true
                            }
                        },
                        RepaymentFrequency = LoanFrequency.Monthly,
                        RepaymentInstallmentAmount = 25000
                    }
            };

            await Assert.ThrowsAsync<InvalidArgumentException>(async () => await service.SetSelectedOffer("1", "f01", "initialoffer1"));
          

        }
        [Fact]
        public async void ApplicationService_SetFinalOffer_ThrowsExceoptionOnInvalidData()
        {
            List<IApplication> application = GetApplicationData();

            var fakeApplicantService = GetApplicantService();
            var fakeApplicationRepository = GetFakeApplicationRepository(application);
            var service = GetApplicationService(fakeApplicantService, fakeApplicationRepository);

            var finalOffers = new List<ILoanOffer>()
            {
                 new LoanOffer()
                    {
                        AnnualPercentageRate = 0,
                        DisbursedLoanAmount = 0,
                        GivenLoanAmount = 0,
                        InterestRate = 0,
                        IsSelectedOffer = false,
                        NumberOfIntallments = 0,
                        OfferFees = new List<IOfferFee>
                        {
                            new OfferFee()
                             {
                                FeeAmount = 10000,
                                FeePercentage = 5,
                                FeeType = OfferFeeType.OneTime,
                                IsIncludedInLoanAmount = true
                            }
                        },
                        RepaymentFrequency = LoanFrequency.Monthly,
                        RepaymentInstallmentAmount = 0
                    },
                  new LoanOffer()
                    {
                        AnnualPercentageRate = 10.23,
                        DisbursedLoanAmount = 199000,
                        GivenLoanAmount = 200000,
                        InterestRate = 12,
                        IsSelectedOffer = false,
                        NumberOfIntallments = 36,
                        OfferFees = new List<IOfferFee>
                        {
                            new OfferFee()
                             {
                                FeeAmount = 10000,
                                FeePercentage = 5,
                                FeeType = OfferFeeType.OneTime,
                                IsIncludedInLoanAmount = true
                            }
                        },
                        RepaymentFrequency = LoanFrequency.Monthly,
                        RepaymentInstallmentAmount = 25000
                    }
            };

            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.SetOffers(null, "finaloffer", finalOffers));
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.SetOffers("1", "", finalOffers));
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.SetOffers("1", "finaloffer", null));
            await Assert.ThrowsAsync<ArgumentException>(async () => await service.SetOffers("1", "offer", finalOffers));
            await Assert.ThrowsAsync<ArgumentException>(async () => await service.SetOffers("1", "finaloffer", finalOffers));
        }

        [Fact]
        public async void ApplicationService_SetSelectedInitialOffer_Success()
        {
            List<IApplication> application = GetApplicationData();

            var fakeApplicantService = GetApplicantService();
            var fakeApplicationRepository = GetFakeApplicationRepository(application);
            var service = GetApplicationService(fakeApplicantService, fakeApplicationRepository);

            await service.SetSelectedOffer("1", "f01", "initialoffer");
            Assert.Equal(1, fakeApplicationRepository.Applications.First().FinalOffers.Count);
            Assert.True(fakeApplicationRepository.Applications.First().InitialOffers.Where(f => f.OfferId == "f01").First().IsSelectedOffer);
           

            
        }


        [Fact]
        public async void ApplicationService_SetSelectedFinalOffer_Success()
        {
            List<IApplication> application = GetApplicationData();

            var fakeApplicantService = GetApplicantService();
            var fakeApplicationRepository = GetFakeApplicationRepository(application);
            var service = GetApplicationService(fakeApplicantService, fakeApplicationRepository);

            await service.SetSelectedOffer("1", "f01", "finaloffer");
            Assert.Equal(1, fakeApplicationRepository.Applications.First().FinalOffers.Count);
            Assert.True(fakeApplicationRepository.Applications.First().InitialOffers.Where(f => f.OfferId == "f01").First().IsSelectedOffer);



        }
        [Fact]
        public async void ApplicationService_SetSelectedInitialOffer_ThrowsExceoptionOnInvalidData()
        {
            List<IApplication> application = GetApplicationData();

            var fakeApplicantService = GetApplicantService();
            var fakeApplicationRepository = GetFakeApplicationRepository(application);
            var service = GetApplicationService(fakeApplicantService, fakeApplicationRepository);

            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.SetSelectedOffer(null, "f01", "finaloffer"));
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.SetSelectedOffer("1", "", "finaloffer"));
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.SetSelectedOffer("1", "f01", null));
        }


        #endregion

        #region Private Methods
        private static FakeApplicantService GetApplicantService(IEnumerable<IApplicant> applicants = null)
        {
            return new FakeApplicantService(new UtcTenantTime(), applicants ?? new List<IApplicant>());
        }

        private static FakeApplicationRepository GetFakeApplicationRepository(List<IApplication> applications = null)
        {
            return new FakeApplicationRepository(new UtcTenantTime(), applications ?? new List<IApplication>());
        }

        private static IApplicationService GetApplicationService(IApplicantService applicantService)
        {
            return new ApplicationService(applicantService, GetFakeApplicationRepository(), new FakeApplicationNumberGenerator(),
                Mock.Of<ILogger>(), Mock.Of<IEventHubClient>(), new ApplicationConfiguration() { ExpiryDays = 60, InitialLeadStatus = "200.01", InitialStatus = "200.02" }, Mock.Of<ITenantTime>());
        }

        private static IApplicationService GetApplicationService(IApplicantService applicantService, IApplicationRepository fakeApplicationRepository)
        {
            return new ApplicationService(applicantService, fakeApplicationRepository, new FakeApplicationNumberGenerator(),
                Mock.Of<ILogger>(), Mock.Of<IEventHubClient>(), new ApplicationConfiguration() { ExpiryDays = 60, InitialLeadStatus = "200.01", InitialStatus = "200.02" }, Mock.Of<ITenantTime>());
        }

        private static IApplicationService GetApplicationService(List<IApplication> applications = null, IEnumerable<IApplicant> applicants = null)
        {
            return new ApplicationService(GetApplicantService(applicants),
                new FakeApplicationRepository(new UtcTenantTime(), applications ?? new List<IApplication>()), new FakeApplicationNumberGenerator(),
                Mock.Of<ILogger>(), Mock.Of<IEventHubClient>(), new ApplicationConfiguration() { ExpiryDays = 60, InitialLeadStatus = "200.01", InitialStatus = "200.02" }, Mock.Of<ITenantTime>());
        }

        private static IApplicationService GetApplicationService(IApplicantService applicantService, List<IApplication> applications = null)
        {
            return new FakeApplicationService(new UtcTenantTime(), new FakeApplicationNumberGenerator(), new ApplicationConfiguration() { ExpiryDays = 60, InitialLeadStatus = "200.01", InitialStatus = "200.02" }, applications);
        }

        private IApplicationRequest GetApplicationRequestData()
        {
            return new ApplicationRequest
            {
                PrimaryApplicant = new ApplicantRequest()
                {
                    PrimaryAddress = new LendFoundry.Business.Applicant.India.Address(),
                    PrimaryEmail = new LendFoundry.Business.Applicant.India.EmailAddress(),
                    PrimaryFax = "test",
                    PrimaryPhone = new LendFoundry.Business.Applicant.India.PhoneNumber(),
                    Addresses = new List<Applicant.IAddress>
                     {
                         new Applicant.Address()
                         {
                             AddressLine1 = "Test1",
                             AddressLine2 = "Test2",
                             City = "Ahmedabad",
                             State = "Gujarat",
                             PinCode = "380001",
                             Country = "India",
                              AddressType = Applicant.AddressType.Home
                         }
                     },

                    EmailAddresses = new List<Applicant.IEmailAddress>
                     {
                         new Applicant.EmailAddress
                         {
                             Email = "Test@SigmaInfo.net",
                             EmailType = Applicant.EmailType.Personal,
                             IsDefault = true
                         }
                     },
                    UserName = "TestUserExists",
                    Password = "TestPassword",
                },
                PurposeOfLoan = PurposeOfLoan.DebtConsolidation,
                Source = new Source()
                {
                    SourceType = SourceType.Merchant,

                    TrackingCode = "Code123",
                    SourceReferenceId = "Refe111"
                },

                RequestedAmount = 10000,
                RequestedTermType = LoanFrequency.Monthly,
                RequestedTermValue = 12.4333
            };
        }

        private static List<IApplication> GetApplicationData()
        {
            return new List<IApplication>
            {

                new Application()
            {

                Source = new Source()
                {
                    SourceType = SourceType.Merchant,

                    TrackingCode = "Code123",
                    SourceReferenceId = "Refe123"
                },

                RequestedAmount = 10000,
                RequestedTermType = LoanFrequency.Monthly,
                RequestedTermValue = 12.4333,
                ApplicationNumber = "1",
                ApplicationStatus = "200.02",
                LinkedBankInformation = new BankInformation()
                {
                        BankName = "HDFC",
                        AccountNumber = "567907845",
                        AccountHolderName = "SigmaInfo",
                        RoutingNumber = "ifsc1234",
                        AccountType =  AccountType.Savings
                },
                LinkedEmailAddress = new EmailAddress()
                {
                    Email = "myemail@sigmainfo.net",
                    IsDefault = true,
                    IsVerified = false,
                    EmailType = EmailType.Work
                },
              SelfDeclareInformation = new SelfDeclareInformation()
                {
                    AnnualRevenue =1000,
                    SelfReportedAnnualRevenue = 500000,
                      AverageBankBalance = 2000,
                       IsExistingBusinessLoan = true
                },
                InitialOffers = new List<ILoanOffer>
                {
                    new LoanOffer()
                    {
                        AnnualPercentageRate = 10.23,
                        DisbursedLoanAmount = 199000,
                        GivenLoanAmount = 200000,
                        InterestRate = 10,
                        IsSelectedOffer = true,
                        NumberOfIntallments = 10,
                        OfferId = "f01",
                        OfferFees = new List<IOfferFee>
                        {
                            new OfferFee()
                             {
                                FeeAmount = 10000,
                                FeePercentage = 5,
                                FeeType = OfferFeeType.OneTime,
                                IsIncludedInLoanAmount = true
                            }
                        },
                        RepaymentFrequency = LoanFrequency.Monthly,
                        RepaymentInstallmentAmount = 15000
                    }
                },
                 FinalOffers = new List<ILoanOffer>
                {
                    new LoanOffer()
                    {
                        AnnualPercentageRate = 10.23,
                        DisbursedLoanAmount = 199000,
                        GivenLoanAmount = 200000,
                        InterestRate = 10,
                        IsSelectedOffer = false,
                        NumberOfIntallments = 10,
                        OfferFees = new List<IOfferFee>
                        {
                            new OfferFee()
                             {
                                FeeAmount = 10000,
                                FeePercentage = 5,
                                FeeType = OfferFeeType.OneTime,
                                IsIncludedInLoanAmount = true
                            }
                        },

                        RepaymentFrequency = LoanFrequency.Monthly,
                        RepaymentInstallmentAmount = 15000
                    }
                }
            }};
        }

        #endregion
    }
}
