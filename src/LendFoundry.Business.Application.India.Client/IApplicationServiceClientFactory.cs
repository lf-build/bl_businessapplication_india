﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.Business.Application.India.Client
{
    public interface IApplicationServiceClientFactory
    {
        IApplicationService Create(ITokenReader reader);
    }
}
