﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.Foundation.Client;
using LendFoundry.InstantBankVerification;
using LendFoundry.ProductConfiguration;
using RestSharp;

namespace LendFoundry.Business.Application.India.Client {
    public class ApplicationService : IApplicationService {
        #region Constructors

        public ApplicationService (IServiceClient client) {
            Client = client;
        }

        #endregion Constructors

        #region Private Properties

        private IServiceClient Client { get; }

        #endregion Private Properties

        #region Public Methods

        public async Task<IApplication> GetByApplicationNumber (string applicationNumber) {
            var request = new RestRequest ("/{applicationNumber}", Method.GET);
            request.AddUrlSegment ("applicationNumber", applicationNumber);
            return await Client.ExecuteAsync<Application> (request);
        }

        public async Task<IApplication> GetByApplicationId (string applicationNumber) {
            var request = new RestRequest ("/{applicationNumber}", Method.GET);
            request.AddUrlSegment ("applicationNumber", applicationNumber);
            return await Client.ExecuteAsync<Application> (request);
        }

        public async Task<IApplicationResponse> Add (IApplicationRequest applicationRequest) {
            var request = new RestRequest ("/", Method.POST);
            request.AddJsonBody (applicationRequest);
            return await Client.ExecuteAsync<ApplicationResponse> (request);
        }

        public async Task<IApplicationResponse> AddMerchantApplication (IApplicationRequest applicationRequest) {
            var request = new RestRequest ("/lead", Method.POST);
            request.AddJsonBody (applicationRequest);
            return await Client.ExecuteAsync<ApplicationResponse> (request);
        }

        public async Task<IApplicationResponse> UpdateApplication (string applicationId, IApplicationRequest applicationRequest) {
            var request = new RestRequest ("/{applicationNumber}", Method.PUT);
            request.AddUrlSegment ("applicationNumber", applicationId);
            request.AddJsonBody (applicationRequest);
            return await Client.ExecuteAsync<ApplicationResponse> (request);
        }

        public async Task LinkBankInformation (string applicationNumber, IBankInformation bankInformationRequest) {
            var request = new RestRequest ("/{applicationNumber}/bankinformation", Method.PUT);
            request.AddUrlSegment ("applicationId", applicationNumber);
            request.AddJsonBody (bankInformationRequest);
            await Client.ExecuteAsync (request);
        }

        public async Task LinkEmailInformation (string applicationNumber, IEmailAddress emailInformationRequest) {
            var request = new RestRequest ("/{applicationNumber}/emailinforamtion", Method.PUT);
            request.AddUrlSegment ("applicationNumber", applicationNumber);
            request.AddJsonBody (emailInformationRequest);
            await Client.ExecuteAsync (request);
        }

        public async Task SetSelfDeclaredInformation (string applicationNumber, ISelfDeclareInformation declareInformationRequest) {
            var request = new RestRequest ("/{applicationNumber}/declaredexpense", Method.PUT);
            request.AddUrlSegment ("applicationNumber", applicationNumber);
            request.AddJsonBody (declareInformationRequest);
            await Client.ExecuteAsync (request);
        }

        public async Task<IApplicationDetails> GetApplicationDetails (string applicationNumber) {
            var request = new RestRequest ("/{applicationNumber}/details", Method.GET);
            request.AddUrlSegment ("applicationNumber", applicationNumber);
            return await Client.ExecuteAsync<ApplicationDetails> (request);
        }

        public async Task<IApplication> AddExternalSources (string applicationNumber, IExternalReferencesRequest externalReferences) {
            var request = new RestRequest ("{applicationNumber}/externalreferences", Method.POST);
            request.AddUrlSegment ("applicationNumber", applicationNumber);
            request.AddJsonBody (externalReferences);
            return await Client.ExecuteAsync<Application> (request);
        }

        public async Task<List<IExternalReferences>> GetExternalSources (string applicationNumber) {
            var request = new RestRequest ("{applicationNumber}/externalreferences", Method.GET);
            request.AddUrlSegment ("applicationNumber", applicationNumber);
            return await Client.ExecuteAsync<List<IExternalReferences>> (request);
        }

        public async Task<List<IApplication>> GetApplicationByApplicantId (string applicantId) {
            var request = new RestRequest ("application/by/{applicantId}", Method.GET);
            request.AddUrlSegment ("applicantId", applicantId);
            return await Client.ExecuteAsync<List<IApplication>> (request);
        }

        public async Task<IApplication> SwitchProduct (string applicationNumber, string productId, string otherLendersNotes) {
            var request = new RestRequest ("{applicationNumber}/switchProduct/{productId}", Method.POST);
            request.AddUrlSegment ("applicationNumber", applicationNumber);
            request.AddUrlSegment ("productId", productId);
            request.AddJsonBody (otherLendersNotes);
            return await Client.ExecuteAsync<Application> (request);
        }

        public async Task UpdateDrawDownAmount (string applicationNumber, IUpdateDrawDownAmountRequest requestAmount) {
            var request = new RestRequest ("{applicationNumber}/drawdownAmount", Method.PUT);
            request.AddUrlSegment ("applicationNumber", applicationNumber);
            request.AddJsonBody (requestAmount);
            await Client.ExecuteAsync (request);
        }

        public async Task UpdateLoanAmount (string applicationNumber, IUpdateLoanAmountRequest requestLoanAmount) {
            var request = new RestRequest ("{applicationNumber}/loanAmount", Method.PUT);
            request.AddUrlSegment ("applicationNumber", applicationNumber);
            request.AddJsonBody (requestLoanAmount);
            await Client.ExecuteAsync (request);
        }
        public async Task<IApplication> UpdateFields (string applicationId, Dictionary<string, object> applicationFields) {
            var request = new RestRequest ("{applicationId}/update/fields", Method.PUT);
            request.AddUrlSegment ("applicationId", applicationId);
            request.AddJsonBody (applicationFields);
            return await Client.ExecuteAsync<Application> (request);
        }

        public async Task SetPrimary (string applicationId, string ownerId) {
            var request = new RestRequest ("{applicationId}/set/{ownerId}", Method.PUT);
            request.AddUrlSegment ("applicationId", applicationId);
            request.AddUrlSegment ("ownerId", ownerId);
            await Client.ExecuteAsync (request);
        }

        public async Task<string> InitiateWorkFlow (string productId, string applicationNumber, IProduct product, WorkFlowType WorkflowType = WorkFlowType.ApplicationApproval) {
            var request = new RestRequest ("initiateWorkFlow/{productId}/{applicationNumber}/{WorkflowType?}", Method.POST);
            request.AddUrlSegment ("productId", productId);
            request.AddUrlSegment ("applicationNumber", applicationNumber);
            request.AddUrlSegment ("WorkflowType", WorkflowType);
            request.AddJsonBody (product);
            return await Client.ExecuteAsync<string> (request);
        }
        public async Task<dynamic> SaveProductAttribute (string ProductId, IProduct product) {
            var request = new RestRequest ("saveProductAttribute/{ProductId}", Method.POST);
            request.AddUrlSegment ("ProductId", ProductId);
            request.AddJsonBody (product);
            return await Client.ExecuteAsync<dynamic> (request);
        }
        public async Task<IApplicationResponse> AddRenewalApplication (string applicationNumber, IApplicationRequest applicationRequest) {
            var request = new RestRequest ("add/renewal/{applicationNumber}", Method.POST);
            request.AddUrlSegment ("applicationNumber", applicationNumber);
            request.AddJsonBody (applicationRequest);
            return await Client.ExecuteAsync<ApplicationResponse> (request);
        }

        public async Task<List<IApplication>> GetRelatedApplications (string applicationNumber) {
            var request = new RestRequest ("related/applications/{applicationNumber}", Method.GET);
            request.AddUrlSegment ("applicationNumber", applicationNumber);
            var applicationDetails = await Client.ExecuteAsync<List<Application>> (request);
            return applicationDetails.ToList<IApplication> ();
        }
        public async Task<bool> AddAccountPreference (string entityType, string entityId, IAccountPreferenceRequest accountPreference) {
            var request = new RestRequest ($"/{entityType}/{entityId}/add/account/preference", Method.POST);
            request.AddJsonBody (accountPreference);
            return await Client.ExecuteAsync<bool> (request);
        }
        public async Task<IBankAccount> AddManualDetails (string entityType, string entityId, IRequestManualBankLink requestManualBankLinkRequest) {
            var request = new RestRequest ($"/{entityType}/{entityId}/manual/details", Method.POST);
            request.AddJsonBody (requestManualBankLinkRequest);
            return await Client.ExecuteAsync<BankAccount> (request);
        }

        public async Task AddBankLink (string bankLinkId, string entityType, string entityId) {
            var request = new RestRequest ($"/{bankLinkId}/{entityType}/{entityId}/addBankLinks", Method.GET);
            await Client.ExecuteAsync (request);
        }
        public async Task<IAccountTypeResponse> GetAccountByType (string entityType, string entityId, IAccountTypeRequest accountTypeRequest) {
            var request = new RestRequest ($"/{entityType}/{entityId}/accountTypeSet", Method.POST);
            request.AddJsonBody (accountTypeRequest);
            return await Client.ExecuteAsync<AccountTypeResponse> (request);
        }

        #endregion Public Methods
    }
}