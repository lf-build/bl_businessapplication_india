using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Dynamic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using LendFoundry.Business.Applicant.India;
using LendFoundry.Business.Application.India.Events;
using LendFoundry.Business.Application.India.Persistence;
using LendFoundry.DataAttributes;
using LendFoundry.EventHub;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Foundation.Services;
using LendFoundry.InstantBankVerification;
using LendFoundry.NumberGenerator;
using LendFoundry.ProductConfiguration;
using LendFoundry.StatusManagement;
namespace LendFoundry.Business.Application.India {
    public class ApplicationService : IApplicationService {
        #region Constructor

        public ApplicationService
            (
                IApplicantService applicantService,
                IApplicationRepository applicationRepository,
                IGeneratorService applicationNumberGenerator,
                ILogger logger,
                ILookupService lookupService,
                IEventHubClient eventHubClient,
                ApplicationConfiguration applicationConfiguration,
                ITenantTime tenantTime,
                IProductService productService,
                IEntityStatusService entityStatusService,
                IDataAttributesEngine dataAttributesEngine,
                IBanklinkRepository bankLinkRepository,
                IBankaccountRepository bankAccountRepository
            ) {
                ApplicantService = applicantService;
                ApplicationRepository = applicationRepository;
                ApplicationNumberGenerator = applicationNumberGenerator;
                EventHubClient = eventHubClient;
                CommandExecutor = new CommandExecutor (logger);
                if (applicationConfiguration == null)
                    throw new ArgumentNullException (nameof (applicationConfiguration));
                ApplicationConfigurations = applicationConfiguration;
                TenantTime = tenantTime;
                LookupService = lookupService;
                ProductService = productService;
                EntityStatusService = entityStatusService;
                DataAttributesEngine = dataAttributesEngine;
                Logger = logger;
                BankLinkRepository = bankLinkRepository;
                BankAccountRepository = bankAccountRepository;
            }

        #endregion Constructor

        #region Private Properties
        private IDataAttributesEngine DataAttributesEngine { get; }

        private IProductService ProductService { get; }
        private ILookupService LookupService { get; }
        private IApplicantService ApplicantService { get; }
        private IEntityStatusService EntityStatusService { get; }
        private IApplicationRepository ApplicationRepository { get; }
        private IGeneratorService ApplicationNumberGenerator { get; }
        private IEventHubClient EventHubClient { get; }
        private ITenantTime TenantTime { get; }
        private CommandExecutor CommandExecutor { get; }
        private ApplicationConfiguration ApplicationConfigurations { get; }
        private ILogger Logger { get; }
        private IBanklinkRepository BankLinkRepository { get; }
        private IBankaccountRepository BankAccountRepository { get; }

        #endregion Private Properties

        #region PrivateMethods

        private async Task<IApplicationExtension> SaveApplication (IApplicationRequest request, string applicationStatus) {
            Application application = null;
            IApplicant applicant = null;
            if (string.IsNullOrWhiteSpace (request.ProductId))
                request.ProductId = ApplicationConfigurations.DefaultProductId;
            var product = await ProductService.Get (request.ProductId);

            if (product.ValidFrom.Time > TenantTime.Now || product.ExpiresOn.Time < TenantTime.Now)
                throw new Exception ("Product is Expired");

            var applicationExtension = new ApplicationExtension ();
            string ApplicantId = string.Empty;
            if (string.IsNullOrEmpty (request.ApplicantId)) {
                await Task.Run (() => {

                    application = new Application (request);
                    var addApplicantCommand = new Command (
                        execute: () => {
                            ApplicantRequest applicantRequest = new ApplicantRequest (request.PrimaryApplicant);
                            applicant = ApplicantService.Add (applicantRequest).Result;
                            applicationExtension.Applicant = applicant;
                            ApplicantId = applicant.Id;
                        },
                        rollback: () => ApplicantService.Delete (application.ApplicantId)
                    );
                    CommandExecutor.Execute (new List<Command> () { addApplicantCommand });
                });
            } else {
                ApplicantId = request.ApplicantId;
                applicationExtension.Applicant = await ApplicantService.Get (ApplicantId);
            }
            string applicantNumber = ApplicationNumberGenerator.TakeNext ("application").Result;
            request.OriginalParentApplicationNumber = applicantNumber;
            request.ParentApplicationNumber = applicantNumber;
            if (string.IsNullOrWhiteSpace (applicantNumber))
                throw new Exception ("Unable to generate application number");
            var Owners = SetOwners (applicationExtension.Applicant.Owners);
            var applicationResult = await AddApplication (request, applicantNumber, Owners, ApplicantId);
            var statusWorkFlowId = await InitiateWorkFlow (applicationResult.Application.ProductId, applicationResult.Application.ApplicationNumber, product);
            applicationResult.StatusWorkFlowId = statusWorkFlowId;
            applicationResult.Applicant = applicationExtension.Applicant;
            return applicationResult;
        }

        private List<string> SetOwners (IList<IOwner> owners) {
            List<string> Owner = new List<string> ();
            if (owners != null && owners.Any ()) {
                foreach (var ownerdata in owners) {
                    Owner.Add (ownerdata.OwnerId);
                }
            }
            return Owner;
        }

        private async Task<IApplicant> GetApplicant (string applicantId) {
            if (string.IsNullOrWhiteSpace (applicantId))
                throw new ArgumentNullException ($"{nameof(applicantId)} cannot be null");

            var applicant = await ApplicantService.Get (applicantId);

            if (applicant == null)
                throw new NotFoundException ($"Applicant {applicant} not found");

            return applicant;
        }

        private async Task<IApplicationExtension> SaveRenewalApplication (string applicationNumber, IApplicationRequest request, string applicationStatus) {
            var applicationDetail = await GetByApplicationNumber (applicationNumber);
            if (string.IsNullOrWhiteSpace (request.ProductId))
                request.ProductId = applicationDetail.ProductId;

            var product = await ProductService.Get (request.ProductId);

            if (product.ValidFrom.Time > TenantTime.Now || product.ExpiresOn.Time < TenantTime.Now)
                throw new Exception ("Product is Expired");

            request.OriginalParentApplicationNumber = (string.IsNullOrWhiteSpace (applicationDetail.ParentApplicationNumber)) ? applicationDetail.ApplicationNumber : applicationDetail.OriginalParentApplicationNumber;
            var Owners = applicationDetail.Owners;
            var renewalApplicationNumber = ApplicationNumberGenerator.TakeNext ("renewalApplication", new List<string> () { request.OriginalParentApplicationNumber }).Result;
            string newapplicationNumber = request.OriginalParentApplicationNumber + "-" + renewalApplicationNumber;
            request.ParentApplicationNumber = applicationNumber;
            request.ApplicationType = ApplicationType.Renewal.ToString ();
            var applicationExtension = await AddApplication (request, newapplicationNumber, Owners, applicationDetail.ApplicantId);
            var statusWorkFlowId = await InitiateWorkFlow (applicationDetail.ProductId, newapplicationNumber, product, WorkFlowType.RenewalApplicationApproval);
            applicationExtension.StatusWorkFlowId = statusWorkFlowId;
            applicationExtension.Applicant = await GetApplicant (applicationDetail.ApplicantId);
            return applicationExtension;
        }

        private async Task<IApplicationExtension> AddApplication (IApplicationRequest request, string applicationNumber, List<string> owners, string applicantId) {
            Application application = null;
            // IApplicant applicant = null;
            if (string.IsNullOrWhiteSpace (request.ProductId))
                request.ProductId = ApplicationConfigurations.DefaultProductId;
            var product = await ProductService.Get (request.ProductId);

            if (product.ValidFrom.Time > TenantTime.Now || product.ExpiresOn.Time < TenantTime.Now)
                throw new Exception ("Product is Expired");

            var applicationExtension = new ApplicationExtension ();
            await Task.Run (() => {
                application = new Application (request);

                var addApplicationCommand = new Command (
                    execute: () => {
                        application.Owners = owners;
                        application.ApplicantId = applicantId;
                        application.ApplicationNumber = applicationNumber;
                        application.PortfolioType = Convert.ToString (product.PortfolioType);
                        application.ProductCategory = Convert.ToString (product.ProductCategory);
                        // application.ApplicationDate = new TimeBucket (TenantTime.Now);
                        application.ExpiryDate = new TimeBucket (TenantTime.Now.AddDays (ApplicationConfigurations.ExpiryDays));
                        ApplicationRepository.Add (application);
                        applicationExtension.Application = application;
                    },
                    rollback: () => ApplicationRepository.Remove (application)
                );
                CommandExecutor.Execute (new List<Command> () { addApplicationCommand });
            });

            var productData = await SaveProductAttribute (request.ProductId, product);

            await DataAttributesEngine.SetAttribute ("application", applicationExtension.Application.ApplicationNumber, "product", (object) productData);

            return applicationExtension;
        }

        #endregion PrivateMethods

        #region PublicMethods

        public async Task<IApplicationResponse> Add (IApplicationRequest request) {
            IApplicationExtension applicationExtension = await SaveApplication (request, ApplicationConfigurations.InitialStatus);

            await EventHubClient.Publish (new ApplicationCreated () {
                Application = applicationExtension.Application,
                    Applicant = applicationExtension.Applicant,
                    EntityId = applicationExtension.Application.ApplicationNumber,
                    EntityType = "application"
            });
            return new ApplicationResponse (applicationExtension.Application, applicationExtension.StatusWorkFlowId, applicationExtension.Applicant.BorrowerId);
        }

        public async Task<IApplicationResponse> AddRenewalApplication (string applicationNumber, IApplicationRequest request) {
            IApplicationExtension applicationExtension = await SaveRenewalApplication (applicationNumber, request, ApplicationConfigurations.InitialStatus);

            await EventHubClient.Publish (new ApplicationCreated () {
                Application = applicationExtension.Application,
                    Applicant = applicationExtension.Applicant,
                    EntityId = applicationExtension.Application.ApplicationNumber,
                    EntityType = "application"
            });
            return new ApplicationResponse (applicationExtension.Application, applicationExtension.StatusWorkFlowId, applicationExtension.Applicant.BorrowerId);
        }

        // Not sure for the Business Application it is needed or not
        public async Task<IApplicationResponse> AddMerchantApplication (IApplicationRequest request) {
            IApplicationExtension applicationExtension = await SaveApplication (request, ApplicationConfigurations.InitialLeadStatus);

            await EventHubClient.Publish (new ApplicationCreated () {
                Application = applicationExtension.Application,
                    Applicant = applicationExtension.Applicant,
                    EntityId = applicationExtension.Application.ApplicationNumber,
                    EntityType = "application"
            });

            return new ApplicationResponse (applicationExtension.Application, applicationExtension.StatusWorkFlowId, applicationExtension.Applicant.BorrowerId);
        }

        public async Task<IApplication> GetByApplicationId (string applicationId) {
            if (string.IsNullOrWhiteSpace (applicationId))
                throw new ArgumentNullException ($"{nameof(applicationId)} cannot be null");

            var application = await ApplicationRepository.GetByApplicationId (applicationId);

            if (application == null)
                throw new NotFoundException ($"Application {applicationId} not found");

            return application;
        }

        public async Task<IApplication> GetByApplicationNumber (string applicationId) {
            if (string.IsNullOrWhiteSpace (applicationId))
                throw new ArgumentNullException ($"{nameof(applicationId)} cannot be null");

            var application = await ApplicationRepository.GetByApplicationId (applicationId);

            if (application == null)
                throw new NotFoundException ($"Application {applicationId} not found");

            return application;
        }

        public async Task LinkBankInformation (string applicationNumber, IBankInformation bankInformationRequest) {
            EnsureInputIsValid (applicationNumber, bankInformationRequest);
            var application = await ApplicationRepository.LinkBankInformation (applicationNumber, bankInformationRequest);
            var applicant = await GetApplicant (application.ApplicantId);
            await EventHubClient.Publish (new ApplicationModified () { Application = application, Applicant = applicant });
        }

        public async Task LinkEmailInformation (string applicationNumber, IEmailAddress emailInformationRequest) {
            EnsureInputIsValid (applicationNumber, emailInformationRequest);
            var application = await ApplicationRepository.LinkEmailInformation (applicationNumber, emailInformationRequest);
            var applicant = await GetApplicant (application.ApplicantId);
            await EventHubClient.Publish (new ApplicationModified () { Application = application, Applicant = applicant });
        }

        public async Task SetSelfDeclaredInformation (string applicationNumber, ISelfDeclareInformation delcareInformationRequest) {
            if (string.IsNullOrWhiteSpace (applicationNumber))
                throw new ArgumentNullException (nameof (applicationNumber));

            if (delcareInformationRequest == null)
                throw new ArgumentNullException (nameof (delcareInformationRequest));

            var application = await ApplicationRepository.SetSelfDeclaredInformation (applicationNumber, delcareInformationRequest);
            var applicant = await GetApplicant (application.ApplicantId);
            await EventHubClient.Publish (new ApplicationModified () { Application = application, Applicant = applicant });
        }

        private async Task<IApplicationExtension> Edit (string applicationNumber, IApplicationRequest request) {
            var applicationExtension = new ApplicationExtension ();
            IApplication application = null;
            IApplicant applicant = null;
            var applicantId = string.Empty;
            if (request.PrimaryApplicant != null && !string.IsNullOrEmpty (request.PrimaryApplicant.Id)) {
                applicantId = request.PrimaryApplicant.Id;
            }
            application = new Application (request);
            application.ApplicationNumber = applicationNumber;
            await Task.Run (() => {
                var updateApplicantCommand = new Command (
                    execute: () => {
                        IUpdateApplicantRequest applicantRequest = new UpdateApplicantRequest (request.PrimaryApplicant);
                        applicant = ApplicantService.UpdateApplicant (applicantId, applicantRequest).Result;
                        applicant.Id = applicantId;
                        applicationExtension.Applicant = applicant;
                        request.PrimaryApplicant.Owners = applicant.Owners;
                    },
                    rollback: () => ApplicantService.Delete (applicantId)
                );
                var updateApplicationCommand = new Command (
                    execute: () => {
                        IApplicationUpdateRequest applicationUpdate = new ApplicationUpdateRequest (request);
                        applicationUpdate.ApplicationDate = new TimeBucket (TenantTime.Now);
                        var objApplication = ApplicationRepository.UpdateApplication (applicationNumber, applicationUpdate);
                        applicationExtension.Application = objApplication.Result;
                    },
                    rollback: () => ApplicationRepository.Remove (application)
                );
                CommandExecutor.Execute (new List<Command> () { updateApplicantCommand, updateApplicationCommand });
            });

            return applicationExtension;
        }

        public async Task<IApplicationResponse> UpdateApplication (string applicationNumber, IApplicationRequest request) {
            if (string.IsNullOrWhiteSpace (applicationNumber))
                throw new Exception ("Application number is mandantory");

            IApplicationExtension applicationExtension = await Edit (applicationNumber, request);

            await EventHubClient.Publish (new ApplicationModified () { Application = applicationExtension.Application, Applicant = applicationExtension.Applicant });

            return new ApplicationResponse (applicationExtension.Application, applicationExtension.StatusWorkFlowId, applicationExtension.Applicant.BorrowerId);
        }

        public async Task<IApplication> AddExternalSources (string applicationNumber, IExternalReferencesRequest externalReferences) {
            Logger.Info ($"Started Add External Source At:{TenantTime.Now.UtcDateTime} for App#:{applicationNumber}");
            var applicationDetails = await GetByApplicationNumber (applicationNumber);

            if (externalReferences == null)
                throw new ArgumentNullException (nameof (externalReferences));

            foreach (var item in externalReferences.ExternalReferences) {

                ExternalReferences objExternalReference = new ExternalReferences ();
                objExternalReference.Name = item.Name;
                objExternalReference.Value = new List<string> ();
                objExternalReference.Value.Add (item.Value);

                if (applicationDetails.ExternalReferences == null) {
                    applicationDetails.ExternalReferences = new List<IExternalReferences> ();
                }

                var isalreadyExists = applicationDetails.ExternalReferences.FirstOrDefault (i => i.Name.ToLower () == item.Name.ToLower ());
                if (isalreadyExists != null)
                    applicationDetails.ExternalReferences.Remove (isalreadyExists);
                applicationDetails.ExternalReferences.Add (objExternalReference);

            }

            ApplicationRepository.Update (applicationDetails);
            Logger.Info ($"Ended Add External Source At:{TenantTime.Now.UtcDateTime} for App#:{applicationNumber}");
            await EventHubClient.Publish (new ApplicationModified () { Application = applicationDetails, Applicant = await GetApplicant (applicationDetails.ApplicantId) });

            return applicationDetails;
        }

        public async Task<List<IExternalReferences>> GetExternalSources (string applicationNumber) {
            var applicationDetails = await GetByApplicationNumber (applicationNumber);

            return applicationDetails.ExternalReferences;
        }

        public async Task<IApplicationDetails> GetApplicationDetails (string applicationId) {
            if (string.IsNullOrWhiteSpace (applicationId))
                throw new ArgumentNullException ($"{nameof(applicationId)} cannot be null");

            var application = await ApplicationRepository.GetByApplicationId (applicationId);

            if (application == null)
                throw new NotFoundException ($"Application {applicationId} not found");

            var applicant = await GetApplicant (application.ApplicantId);

            if (applicant == null)
                throw new NotFoundException ($"Applicant {applicant} not found");

            return new ApplicationDetails (application, applicant);
        }
        public async Task<List<IApplication>> GetApplicationByApplicantId (string applicantId) {
            if (string.IsNullOrWhiteSpace (applicantId))
                throw new ArgumentNullException ($"{nameof(applicantId)} cannot be null");

            return await ApplicationRepository.GetApplicationByApplicantId (applicantId);
        }

        public async Task<IApplication> SwitchProduct (string applicationNumber, string productId, string otherLenderNotes) {
            if (string.IsNullOrWhiteSpace (applicationNumber))
                throw new ArgumentNullException ($"{nameof(applicationNumber)} cannot be null");

            if (string.IsNullOrWhiteSpace (productId))
                throw new ArgumentNullException ($"{nameof(productId)} cannot be null");

            var application = await ApplicationRepository.GetByApplicationId (applicationNumber);
            if (application == null)
                throw new NotFoundException ($"Application {applicationNumber} is not found");

            var product = await ProductService.Get (productId);

            if (product == null)
                throw new NotFoundException ($"Product {productId} is not found");

            var oldProductDataAttributes = await DataAttributesEngine.GetAttribute ("application", applicationNumber, "product");

            var getOldProduct = await ProductService.Get (application.ProductId);

            if (getOldProduct == null)
                throw new NotFoundException ($"Old Product {productId} is not found");

            //var defaultWorkFlowName = getOldProduct.WorkFlow.Where(i => i.WorkFlowType == WorkFlowType.DefaultApproval).FirstOrDefault();
            var statusByProducts = await EntityStatusService.GetStatusByProduct ("application", applicationNumber, application.ProductId);

            await DataAttributesEngine.SetAttribute ("application", applicationNumber, "old-product", oldProductDataAttributes);

            //var currentStatusDetails = await EntityStatusService.GetStatusByEntity("application", applicationNumber, defaultWorkFlowName.WorkFlowName);

            if (statusByProducts != null && statusByProducts.Count > 0) {
                if (statusByProducts.FirstOrDefault ().WorkFlowStatus != WorkFlowStatus.Completed)
                    throw new InvalidOperationException ($"Invalid Operation. You can not switch Product");
            }
            var productGroup = await ProductService.GetAllProductGroup (productId);
            var productParameters = await ProductService.GetAllProductParameters (product.ProductId);
            var productFeeParameters = await ProductService.GetAllProductFeeParameters (product.ProductId);
            dynamic productData = new ExpandoObject ();
            productData.ProductId = productId;
            productData.Product = product;
            productData.ProductGroup = productGroup;
            productData.ProductParameter = productParameters;
            productData.ProductCategory = product.ProductCategory;
            productData.PortfolioType = product.PortfolioType;
            productData.ProductFeeParameters = productFeeParameters;
            await DataAttributesEngine.SetAttribute ("application", applicationNumber, "product", (object) productData);

            await InitiateWorkFlow (productId, applicationNumber, product);
            application.ProductId = productId;
            application.ProductCategory = Convert.ToString (product.ProductCategory);
            application.PortfolioType = Convert.ToString (product.PortfolioType);

            application.OldProductId = getOldProduct.ProductId;
            application.OldPortfolioType = Convert.ToString (getOldProduct.PortfolioType);
            application.OldProductCategory = Convert.ToString (getOldProduct.ProductCategory);
            application.OtherLenderNotes = otherLenderNotes;
            application.ProductApprovalDate = new TimeBucket (TenantTime.Now);

            TimeBucket LocExpiryDate = new TimeBucket (TenantTime.Now.AddMonths (ApplicationConfigurations.LocExpiryInMonths));

            await DataAttributesEngine.SetAttribute ("application", applicationNumber, "ProductApprovalDate", application.ProductApprovalDate);
            await DataAttributesEngine.SetAttribute ("application", applicationNumber, "LocExpiryDate", LocExpiryDate);
            ApplicationRepository.Update (application);

            var applicant = await GetApplicant (application.ApplicantId);
            await EventHubClient.Publish (new ApplicationModified () { Application = application, Applicant = applicant });
            return application;
        }

        public async Task<dynamic> SaveProductAttribute (string ProductId, IProduct product) {
            if (string.IsNullOrWhiteSpace (ProductId))
                ProductId = ApplicationConfigurations.DefaultProductId;

            if (product == null || product.ProductId == null || product.WorkFlow == null) {
                product = await ProductService.Get (ProductId);
                if (product.ValidFrom.Time > TenantTime.Now || product.ExpiresOn.Time < TenantTime.Now)
                    throw new Exception ("Product is Expired");
            }

            var productGroup = await ProductService.GetAllProductGroup (ProductId);
            var productParameters = await ProductService.GetAllProductParameters (ProductId);
            var productFeeParameters = await ProductService.GetAllProductFeeParameters (product.ProductId);
            dynamic productData = new ExpandoObject ();
            productData.ProductId = ProductId;
            productData.Product = product;
            productData.ProductGroup = productGroup;
            productData.ProductParameter = productParameters;
            productData.ProductFeeParameters = productFeeParameters;
            product.PortfolioType = product.PortfolioType;
            return productData;
        }
        public async Task<string> InitiateWorkFlow (string productId, string applicationNumber, IProduct product, WorkFlowType WorkFlowType = WorkFlowType.ApplicationApproval) {

            if (string.IsNullOrWhiteSpace (productId))
                throw new ArgumentNullException ($"{nameof(productId)} cannot be null");
            if (string.IsNullOrWhiteSpace (applicationNumber))
                throw new ArgumentNullException ($"{nameof(applicationNumber)} cannot be null");

            string workFlowName = string.Empty;
            var statusDetailsByProduct = await EntityStatusService.GetStatusByProduct ("application", applicationNumber, productId);

            if (statusDetailsByProduct != null && statusDetailsByProduct.Count > 0)
                workFlowName = statusDetailsByProduct.FirstOrDefault ().StatusWorkFlowId;
            else {

                var workFlowDetail = product.WorkFlow.Where (i => i.WorkFlowType == WorkFlowType && i.EntityType == "application").FirstOrDefault ();

                if (workFlowDetail == null)
                    throw new NotFoundException ($"Workflow with the Type {WorkFlowType.ApplicationApproval} not found for {productId}");
                workFlowName = workFlowDetail.WorkFlowName;
            }

            await Task.Run (() => EntityStatusService.ProcessStatusWorkFlow ("application", applicationNumber, productId, workFlowName, WorkFlowStatus.Initiated));

            return workFlowName;
        }
        public async Task UpdateLoanAmount (string applicationNumber, IUpdateLoanAmountRequest requestLoanAmount) {
            if (string.IsNullOrWhiteSpace (applicationNumber))
                throw new ArgumentNullException (nameof (applicationNumber));

            if (requestLoanAmount == null)
                throw new ArgumentNullException (nameof (requestLoanAmount));

            if (requestLoanAmount.LoanAmount <= 0)
                throw new InvalidOperationException ("RequestLoanAmount should be grater than zero");
            var application = await GetByApplicationNumber (applicationNumber);

            if (application == null)
                throw new NotFoundException ($"Application with number {applicationNumber} is not found");

            application.LoanAmount = requestLoanAmount.LoanAmount;
            ApplicationRepository.Update (application);

            var applicant = await ApplicantService.Get (application.ApplicantId);
            await EventHubClient.Publish (new ApplicationModified { Application = application, Applicant = applicant });
        }

        public async Task UpdateDrawDownAmount (string applicationNumber, IUpdateDrawDownAmountRequest requestAmount) {
            if (string.IsNullOrWhiteSpace (applicationNumber))
                throw new ArgumentNullException (nameof (applicationNumber));

            if (requestAmount == null)
                throw new ArgumentNullException (nameof (requestAmount));

            if (requestAmount.DrawDownAmount <= 0)
                throw new InvalidOperationException ("DrawDownAmount should be grater than zero");

            var application = await GetByApplicationNumber (applicationNumber);

            if (application == null)
                throw new NotFoundException ($"Application with number {applicationNumber} is not found");
            var oldLoanAmount = application.RequestedAmount;
            application.TotalDrawDownAmount = requestAmount.DrawDownAmount;
            ApplicationRepository.Update (application);
            var applicant = await ApplicantService.Get (application.ApplicantId);
            await EventHubClient.Publish (new ApplicationModified { Application = application, Applicant = applicant });
        }

        public async Task<IApplication> UpdateFields (string applicationId, Dictionary<string, object> applicationFields) {
            if (string.IsNullOrWhiteSpace (applicationId))
                throw new ArgumentNullException (nameof (applicationId));
            if (applicationFields == null || applicationFields.Count == 0)
                throw new ArgumentNullException (nameof (applicationFields));
            await ApplicationRepository.UpdateFields (applicationId, applicationFields);
            return await GetByApplicationId (applicationId);
        }

        public async Task SetPrimary (string applicationId, string ownerId) {
            if (string.IsNullOrWhiteSpace (applicationId))
                throw new ArgumentNullException (nameof (applicationId));
            if (string.IsNullOrWhiteSpace (ownerId))
                throw new ArgumentNullException (nameof (ownerId));

            var application = await ApplicationRepository.GetByApplicationId (applicationId);

            await ApplicantService.SetPrimary (application.ApplicantId, ownerId);

            var applicant = await GetApplicant (application.ApplicantId);
            await EventHubClient.Publish (new ApplicationModified () { Application = application, Applicant = applicant });
        }

        public async Task<List<IApplication>> GetRelatedApplications (string applicationId) {
            if (string.IsNullOrWhiteSpace (applicationId))
                throw new ArgumentNullException ($"{nameof(applicationId)} cannot be null");

            var applications = await ApplicationRepository.GetRelatedApplications (applicationId);

            if (applications == null)
                throw new NotFoundException ($"Application {applicationId} not found");

            return applications;
        }
        #endregion PublicMethods

        #region Validations

        private void EnsureInputIsValid (IApplicationRequest applicationRequest) {
            if (applicationRequest == null)
                throw new ArgumentNullException ("application cannot be empty");

            if (applicationRequest.RequestedAmount <= 0)
                throw new ArgumentNullException ($"Application {nameof(applicationRequest.RequestedAmount)} is mandatory");
            var PurposeOfLoanExists = LookupService.GetLookupEntry ("purposeOfLoan", applicationRequest.PurposeOfLoan);
            if (PurposeOfLoanExists == null)
                throw new InvalidArgumentException ("PurposeOfLoan is not valid");
            var LoantimeFrameExists = LookupService.GetLookupEntry ("loanTimeFrame", applicationRequest.LoanTimeFrame);
            if (LoantimeFrameExists == null)
                throw new InvalidArgumentException ("LoanTimeFrame is not valid");

            var SourceTypeExists = LookupService.GetLookupEntry ("sourceType", applicationRequest.Source.SourceType);
            if (PurposeOfLoanExists == null)
                throw new InvalidArgumentException ("SourceType is not valid");

            if (applicationRequest.RequestedTermType != null) {
                var RequestedTermTypeExists = LookupService.GetLookupEntry ("requestedTermType", applicationRequest.RequestedTermType);

                if (RequestedTermTypeExists == null)
                    throw new InvalidArgumentException ("RequestedTermType is not valid");
            }
            if (applicationRequest.RequestedTermValue >= 0.0) {
                var RequestedTermValueExists = LookupService.GetLookupEntry ("RequestedTermValue", applicationRequest.RequestedTermValue.ToString ());
                if (RequestedTermValueExists == null)
                    throw new InvalidArgumentException ("RequestedTermValue is not valid");
            }
            if (applicationRequest.Source == null)
                throw new ArgumentNullException ($"Application {nameof(applicationRequest.Source)} is mandatory");

            if (string.IsNullOrWhiteSpace (applicationRequest.Source.SourceReferenceId))
                throw new ArgumentNullException ($"Application {nameof(applicationRequest.Source.SourceReferenceId)} is mandatory");

            if (applicationRequest.PrimaryEmail == null)
                throw new ArgumentNullException ($"Applicant {nameof(applicationRequest.PrimaryEmail)} is mandatory");
            if (!Regex.IsMatch (applicationRequest.PrimaryEmail.Email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase))
                throw new ArgumentException ("Email Address is not valid");
            if (applicationRequest.PrimaryPhone == null)
                throw new ArgumentNullException ($"Applicant {nameof(applicationRequest.PrimaryPhone)} is mandatory");

            if (!Regex.IsMatch (applicationRequest.PrimaryPhone.Phone, "^(0|[1-9][0-9]+)$"))
                throw new ArgumentException ("Invalid Phone Number format");

            if (applicationRequest.PrimaryApplicant == null)
                throw new ArgumentNullException ($"Applicant {nameof(applicationRequest.PrimaryApplicant)} is mandatory");

            if (applicationRequest.PrimaryApplicant.Owners == null)
                throw new ArgumentNullException ($"Applicant {nameof(applicationRequest.PrimaryApplicant.Owners)} is mandatory");

            foreach (var Owners in applicationRequest.PrimaryApplicant.Owners) {
                if (string.IsNullOrWhiteSpace (Owners.FirstName))
                    throw new ArgumentNullException ($"Applicant {nameof(Owners.FirstName)} is mandatory");
                if (string.IsNullOrWhiteSpace (Owners.AadhaarId))
                    throw new ArgumentNullException ($"Applicant {nameof(Owners.AadhaarId)} is mandatory");
                if (string.IsNullOrWhiteSpace (Owners.PanId))
                    throw new ArgumentNullException ($"Applicant {nameof(Owners.PanId)} is mandatory");
                if (string.IsNullOrWhiteSpace (Owners.DOB))
                    throw new ArgumentNullException ($"Applicant {nameof(Owners.DOB)} is mandatory");
                if (string.IsNullOrWhiteSpace (Owners.OwnershipPercentage))
                    throw new ArgumentNullException ($"Applicant {nameof(Owners.OwnershipPercentage)} is mandatory");
                if (!Enum.IsDefined (typeof (OwnershipType), Owners.OwnershipType))
                    throw new ArgumentNullException ($"Applicant {nameof(Owners.OwnershipType)} is mandatory");
                if (!Enum.IsDefined (typeof (OwnershipType), Owners.Addresses.Where (x => x.AddressType.ToString () == "Home").Select (x => x.City)))
                    throw new ArgumentNullException ($"Applicant {nameof(Owners.OwnershipType)} is mandatory");
            }
            if (applicationRequest.PrimaryApplicant.Addresses == null)
                throw new ArgumentNullException ($"Applicant {nameof(applicationRequest.PrimaryApplicant.Addresses)} is mandatory");

            foreach (var address in applicationRequest.PrimaryApplicant.Addresses) {
                if (string.IsNullOrWhiteSpace (address.AddressLine1))
                    throw new ArgumentNullException ($"Applicant {nameof(address.AddressLine1)} is mandatory");

                if (string.IsNullOrWhiteSpace (address.City))
                    throw new ArgumentNullException ($"Applicant {nameof(address.City)} is mandatory");

                if (string.IsNullOrWhiteSpace (address.State))
                    throw new ArgumentNullException ($"Applicant {nameof(address.State)} is mandatory");

                if (string.IsNullOrWhiteSpace (address.ZipCode))
                    throw new ArgumentNullException ($"Applicant {nameof(address.ZipCode)} is mandatory");

                if (string.IsNullOrWhiteSpace (address.Country))
                    throw new ArgumentNullException ($"Applicant {nameof(address.Country)} is mandatory");

                if (!Enum.IsDefined (typeof (Applicant.India.AddressType), address.AddressType))
                    throw new InvalidEnumArgumentException ($"{nameof(address.AddressType)} is not valid");
            }

            if (applicationRequest.PrimaryApplicant.EmailAddresses == null)
                throw new ArgumentNullException ($"Applicant {nameof(applicationRequest.PrimaryApplicant.EmailAddresses)} is mandatory");

            foreach (var emailAddress in applicationRequest.PrimaryApplicant.EmailAddresses) {
                if (string.IsNullOrWhiteSpace (emailAddress.Email))
                    throw new ArgumentNullException ($"Applicant {nameof(EmailAddress.Email)} is mandatory");
                if (!Regex.IsMatch (emailAddress.Email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase))
                    throw new ArgumentException ("Email Address is not valid");

                if (!Enum.IsDefined (typeof (Applicant.India.EmailType), emailAddress.EmailType))
                    throw new InvalidEnumArgumentException ($"{nameof(emailAddress.EmailType)} is not valid");
            }
        }

        private void EnsureInputIsValid (string applicationNumber, IBankInformation bankInformationRequest) {
            if (string.IsNullOrWhiteSpace (applicationNumber))
                throw new ArgumentNullException (nameof (applicationNumber));

            if (bankInformationRequest == null)
                throw new ArgumentNullException (nameof (bankInformationRequest));

            if (string.IsNullOrWhiteSpace (bankInformationRequest.BankName))
                throw new ArgumentNullException ($"{nameof(bankInformationRequest.BankName)} is mandatory");

            if (string.IsNullOrWhiteSpace (bankInformationRequest.AccountHolderName))
                throw new ArgumentNullException ($"{nameof(bankInformationRequest.AccountHolderName)} is mandatory");

            if (string.IsNullOrWhiteSpace (bankInformationRequest.AccountNumber))
                throw new ArgumentNullException ($"{nameof(bankInformationRequest.AccountNumber)} is mandatory");

            if (string.IsNullOrWhiteSpace (bankInformationRequest.RoutingNumber))
                throw new ArgumentNullException ($"{nameof(bankInformationRequest.RoutingNumber)} is mandatory");

            if (!Enum.IsDefined (typeof (AccountType), bankInformationRequest.AccountType))
                throw new InvalidEnumArgumentException ($"{nameof(bankInformationRequest.AccountType)} is mandatory");
        }

        private void EnsureInputIsValid (string applicationNumber, IEmailAddress emailInformationRequest) {
            if (string.IsNullOrWhiteSpace (applicationNumber))
                throw new ArgumentNullException (nameof (applicationNumber));

            if (emailInformationRequest == null)
                throw new ArgumentNullException (nameof (emailInformationRequest));

            if (string.IsNullOrWhiteSpace (emailInformationRequest.Email))
                throw new ArgumentNullException ($"{nameof(emailInformationRequest.Email)} is mandatory");

            if (!Enum.IsDefined (typeof (EmailType), emailInformationRequest.EmailType))
                throw new InvalidEnumArgumentException ($"{nameof(emailInformationRequest.EmailType)} is mandatory");
        }

        public async Task<IBankAccount> AddManualDetails (string entityType, string entityId, IRequestManualBankLink request) {

            if (string.IsNullOrWhiteSpace (entityId))
                throw new ArgumentException ($"{nameof(entityId)} is mandatory");

            if (request == null)
                throw new ArgumentNullException ($"{nameof(request)} is nullable");

            var bankLinkResult = await ConvertManualData (entityType, entityId, request);

            if (bankLinkResult == null || string.IsNullOrWhiteSpace (bankLinkResult.Id))
                throw new ArgumentException ("Insertion Issue on banklink");

            var accountData = await ConversionManualAccount (bankLinkResult.Id, request);

            if (request.ManualType.ToUpper () == ManualType.CSVUplaod.ToString ().ToUpper ()) {
                Logger.Info ($"");

            }
            if (request.ManualType.ToUpper () != ManualType.BankLink.ToString ().ToUpper ()) {
                Logger.Info ($"");
            } else {
                await EventHubClient.Publish ("BankDetailsAdded", new {
                    EntityId = entityId,
                        EntityType = entityType,
                        Response = request,
                        ReferenceNumber = Guid.NewGuid ().ToString ("N")
                });
            }
            return accountData;
        }
        private async Task<IBankLink> ConvertManualData (string entityType, string entityId, IRequestManualBankLink request) {
            IBankLink bankLink = new BankLink ();
            return await Task.Run (() => {
                bankLink.FirstName = request.FirstName;
                bankLink.LastName = request.LastName;
                bankLink.UserMailId = request.UserMailId;
                bankLink.EntityId = entityId;
                bankLink.ProviderBankName = request.BankName;
                bankLink.ProviderId = Convert.ToInt32 (Provider.Manual);
                bankLink.ProviderBankId = null;
                bankLink.ProviderBankLinkId = $"{entityId}_{Guid.NewGuid().ToString("N")}";
                bankLink.ProviderToken = Guid.NewGuid ().ToString ("N");
                bankLink.CreatedDate = new TimeBucket (TenantTime.Now);
                ILink link = new Link { EntityType = entityType, Products = null };
                bankLink.Links = new List<ILink> ();
                bankLink.Links.Add (link);
                BankLinkRepository.Add (bankLink);

                return bankLink;
            });

        }

        private async Task<IBankAccount> ConversionManualAccount (string bankLinkId, IRequestManualBankLink request) {
            return await Task.Run (() => {
                IBankAccount bankAccount = new BankAccount ();
                bankAccount.NameOnAccount = null;
                bankAccount.AccountNumberMask = FormatAccountNumber (request.AccountNumber);
                bankAccount.AccountType = request.AccountType;
                bankAccount.AvailableBalance = request.AvailableBalance;
                bankAccount.CurrentBalance = request.CurrentBalance;
                bankAccount.BankLinkId = bankLinkId;
                bankAccount.IsDeleted = false;
                bankAccount.IsActive = true;
                bankAccount.ProviderAccountId = Guid.NewGuid ().ToString ("N");
                bankAccount.CreatedDate = new TimeBucket (TenantTime.Now);
                bankAccount.UpdatedDate = new TimeBucket (TenantTime.Now);
                bankAccount.ProviderId = Convert.ToInt32 (Provider.Manual);
                bankAccount.OwnerName = request.FirstName;
                bankAccount.AccountNumber = request.AccountNumber;
                bankAccount.RoutingNumber = request.RoutingNumber;
                bankAccount.WireRoutingNumber = request.WireRoutingNumber;

                BankAccountRepository.Add (bankAccount);
                return bankAccount;
            });
        }

        private string FormatAccountNumber (string accountNumber) {
            if (!string.IsNullOrWhiteSpace (accountNumber) && accountNumber.Length > 4) {
                return accountNumber.Substring (accountNumber.Length - 4);
            } else {
                return accountNumber;
            }
        }

        public async Task<bool> AddAccountPreference (string entityType, string entityId, IAccountPreferenceRequest accountPreference) {
            if (string.IsNullOrWhiteSpace (entityId))
                throw new ArgumentNullException (nameof (entityId));

            if (accountPreference == null)
                throw new ArgumentNullException (nameof (accountPreference));
            try {

                AccountPreference data = new AccountPreference (accountPreference);
                if (accountPreference.AccountType == "funding") {
                    data.IsCashflowAccount = null;
                }
                if (accountPreference.AccountType == "cashflow") {
                    data.IsFundingAccount = null;
                }
                var bankLinks = await BankLinkRepository.GetBankLinksByEntityId (entityId);

                if (bankLinks == null) {
                    return false;
                }

                var bankLinkIds = bankLinks.Select (x => x.Id).ToList ();

                if (data.IsCashflowAccount.HasValue && data.IsCashflowAccount.Value) {

                    await BankAccountRepository.ResetAccountPreference (bankLinkIds, LendFoundry.InstantBankVerification.AccountType.Cashflow);

                    await EventHubClient.Publish ("CashflowAccountSelected", new {
                        EntityId = entityId,
                            EntityType = entityType,
                            Response = new { dataAttributeName = accountPreference.AccountID },
                            Request = entityId,
                            ReferenceNumber = Guid.NewGuid ().ToString ("N")
                    });
                } else if (data.IsFundingAccount.HasValue && data.IsFundingAccount.Value) {
                    await BankAccountRepository.GetAccountDetails (bankLinkIds, data.AccountID);
                    await BankAccountRepository.ResetAccountPreference (bankLinkIds, LendFoundry.InstantBankVerification.AccountType.Funding);
                    await EventHubClient.Publish ("FundingAccountSelected", new {
                        EntityId = entityId,
                            EntityType = entityType,
                            Response = new { dataAttributeName = accountPreference.AccountID },
                            Request = entityId,
                            ReferenceNumber = Guid.NewGuid ().ToString ("N")
                    });
                }
                return await BankAccountRepository.UpdateAccountPreference (data, accountPreference.AccountVersion);
            } catch (Exception exception) {
                Logger.Error ("Error While Processing AddAccountPreference Date & Time:" + TenantTime.Now + " EntityId:" + entityId + " EntityType:" + "Application" + "Exception" + exception.Message);
                throw;
            }
        }

        public async Task AddBankLink (string bankLinkId, string entityType, string entityId) {
            var bankLinkData = await BankLinkRepository.Get (bankLinkId);

            if (bankLinkData == null) {
                return;
            }

            if (bankLinkData.Links.Any ()) {
                bankLinkData.Links.Add (new Link {
                    EntityId = entityId,
                        EntityType = entityType,
                        Products = null
                });
            } else {
                bankLinkData.Links = new List<ILink> ();
                bankLinkData.Links.Add (new Link {
                    EntityId = entityId,
                        EntityType = entityType,
                        Products = null
                });
            }

            BankLinkRepository.Update (bankLinkData);
        }

        public async Task<IAccountTypeResponse> GetAccountByType (string entityType, string entityId, IAccountTypeRequest request) {
            if (string.IsNullOrWhiteSpace (entityId)) {
                throw new ArgumentNullException (nameof (entityId));
            }

            if (request == null) {
                throw new ArgumentNullException (nameof (request));
            }

            var lstResult = new AccountTypeResponse ();
            var bankLinks = await BankLinkRepository.GetBankLinksByEntityId (entityId);

            if (bankLinks == null) {
                Logger.Info ($"GetAccountByType no banklinks found for {entityId}");
                return null;
            }

            //// TODO : once the cashflow manual trigger done : pass accountId
            var bankLinkFirstData = bankLinks.FirstOrDefault ();
            if (bankLinkFirstData != null) {
                lstResult.BankName = bankLinkFirstData.ProviderBankName;
                lstResult.NameOnAccount = $"{bankLinkFirstData.FirstName} {bankLinkFirstData.LastName}";
            }

            var bankLinkIds = bankLinks.Select (x => x.Id).ToList ();
            try {

                lstResult.accounts = new List<IBankAccount> ();
                if (request.IsCashflowAccount.HasValue && request.IsCashflowAccount.Value) {
                    try {
                        var cashflowAccount = await BankAccountRepository.GetCashflowAccount (bankLinkIds);
                        if (cashflowAccount != null) {
                            lstResult.accounts.Add (cashflowAccount);
                        }
                    } catch (Exception ignored) {
                        Logger.Info (ignored.Message, ignored);
                    }
                }
                if (request.IsFundingAccount.HasValue && request.IsFundingAccount.Value) {
                    try {
                        var fundingAccount = await BankAccountRepository.GetFundingAccount (bankLinkIds);
                        if (fundingAccount != null) {
                            lstResult.accounts.Add (fundingAccount);
                        }
                    } catch (Exception ignored) {
                        Logger.Info (ignored.Message, ignored);
                    }
                }
            } catch (Exception ex) {
                Logger.Info (ex.Message, ex);
            }
            return lstResult;
        }

        #endregion Validations
    }

}