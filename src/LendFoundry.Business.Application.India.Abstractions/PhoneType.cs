﻿namespace LendFoundry.Business.Application.India
{
    public enum PhoneType
    {
        Residence,
        Mobile,
        Work ,
        Alternate,
        Home,
        Others
    }
}