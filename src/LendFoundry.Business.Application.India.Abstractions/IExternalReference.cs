﻿namespace LendFoundry.Business.Application.India
{
    public interface IExternalReference
    {
        string Name { get; set; }
       string Value { get; set; }
    }
}
