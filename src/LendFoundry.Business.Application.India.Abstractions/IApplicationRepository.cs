﻿using LendFoundry.Foundation.Persistence;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.Business.Application.India.Persistence
{
    public interface IApplicationRepository : IRepository<IApplication>
    {
        Task<IApplication> GetByApplicationId(string applicationNumber);

        Task<IApplication> LinkBankInformation(string applicationNumber, IBankInformation bankInformation);

        Task<IApplication> LinkEmailInformation(string applicationNumber, IEmailAddress emailInformation);

        Task<IApplication> SetSelfDeclaredInformation(string applicationNumber, ISelfDeclareInformation declareInformation);

        Task<IApplication> UpdateApplication(string applicationId, IApplicationUpdateRequest applicationRequest);
        Task<List<IApplication>> GetApplicationByApplicantId(string applicantId);
        Task UpdateFields(string applicationNumber, IDictionary<string, object> fields);
        Task<List<IApplication>> GetRelatedApplications(string applicantId);
        
    }
}