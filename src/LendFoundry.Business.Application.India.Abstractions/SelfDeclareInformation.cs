﻿namespace LendFoundry.Business.Application.India
{
    public class SelfDeclareInformation : ISelfDeclareInformation
    {
        public SelfDeclareInformation() { }

        public SelfDeclareInformation(ISelfDeclareInformation expenseRequest)
        {
           
            AnnualRevenue = expenseRequest.AnnualRevenue;
            SelfReportedAnnualRevenue = expenseRequest.SelfReportedAnnualRevenue;
            AverageBankBalance = expenseRequest.AverageBankBalance;
            IsExistingBusinessLoan = expenseRequest.IsExistingBusinessLoan;
        }
      
        public double AnnualRevenue { get; set; }
        public double SelfReportedAnnualRevenue { get; set; }
        public double AverageBankBalance { get; set; }
        public bool IsExistingBusinessLoan { get; set; }
    }
}
