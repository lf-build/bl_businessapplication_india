﻿
namespace LendFoundry.Business.Application.India
{
    public interface IEmailAddress
    {
        string Email { get; set; }
        EmailType EmailType { get; set; }      
        bool IsDefault { get; set; }
      
    }
}
