﻿namespace LendFoundry.Business.Application.India
{
    public class Source : ISource
    {
        public string TrackingCode { get; set; }

        //    public SystemChannel SystemChannel { get; set; } May be use in future

        public string SourceType { get; set; }

        public string SourceReferenceId { get; set; }
    }
}