﻿using System;

namespace LendFoundry.Business.Application.India
{
    public static class Settings
    {
        public static string ServiceName => Environment.GetEnvironmentVariable($"CONFIGURATION_NAME") ?? "business-application";
       
    }
}