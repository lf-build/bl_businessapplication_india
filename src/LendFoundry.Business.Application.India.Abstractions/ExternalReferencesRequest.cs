﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.Business.Application.India
{
    public class ExternalReferencesRequest : IExternalReferencesRequest
    {
        [JsonConverter(typeof(InterfaceListConverter<IExternalReference, ExternalReference>))]
        public   List<IExternalReference> ExternalReferences { get; set; }
    }
}
