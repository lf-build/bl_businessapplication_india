﻿namespace LendFoundry.Business.Application.India
{
    public class Address : IAddress
    {
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }
        public string AddressLine4 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string Country { get; set; }
        public AddressType AddressType { get; set; }
        public bool IsDefault { get; set; }
        public string LandMark { get; set; }
        public string Location { get; set; }
    }
}