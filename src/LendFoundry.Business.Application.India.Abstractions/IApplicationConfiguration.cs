﻿using LendFoundry.Foundation.Client;
using System.Collections.Generic;
namespace LendFoundry.Business.Application.India
{
    public interface  IApplicationConfiguration : IDependencyConfiguration
    {
        string InitialStatus { get; set; }

        int LocExpiryInMonths { get; set; }
        string ConnectionString { get; set; }
    }
}
