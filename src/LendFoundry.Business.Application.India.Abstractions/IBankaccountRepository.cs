 using System.Collections.Generic;
 using System.Threading.Tasks;
 using LendFoundry.Foundation.Persistence;
 using LendFoundry.InstantBankVerification;

 namespace LendFoundry.Business.Application.India.Persistence {
     public interface IBankaccountRepository : IRepository<IBankAccount> {
         Task<bool> AccountExists (string accountId);
         Task AddAccount (IBankAccount account);
         Task<IBankAccount> GetAccount (string bankLinkId, string accountNumber);
         Task<IBankAccount> GetAccount (string bankLinkId);
         Task<IBankAccount> GetAccountByProviderAccountId (string accountId);
         Task<IBankAccount> GetAccountDetails (List<string> bankLinkIds, string accontId);
         Task<List<string>> GetAccountIds (string bankLinkId);
         Task<IList<IBankAccount>> GetAccounts (string bankLinkId);
         Task<IList<IBankAccount>> GetAccountsByEntityId (List<string> bankLinkIds);
         Task<IBankAccount> GetCashflowAccount (List<string> bankLinkIds);
         Task<IBankAccount> GetFundingAccount (List<string> bankLinkIds);
         Task<IBankAccount> GetSelectedAccountsByBankLinkId (string bankLinkId);
         Task<bool> ResetAccountPreference (List<string> bankLinkIds, LendFoundry.InstantBankVerification.AccountType accountType);
         Task<bool> UpdateAccount (IBankAccount account);
         Task<bool> UpdateAccountPreference (IAccountPreference request, int? accountVersion);
         Task<bool> UpdateBankAccount (string bankLinkId, string accountNumber);
         Task<bool> UpdateBankAccount (string bankLinkId);

     }
 }