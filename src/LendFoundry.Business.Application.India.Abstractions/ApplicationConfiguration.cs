﻿using LendFoundry.Foundation.Client;
using System.Collections.Generic;

namespace LendFoundry.Business.Application.India
{
    public class ApplicationConfiguration : IApplicationConfiguration, IDependencyConfiguration
    {
        public string InitialStatus { get; set; }
        public string InitialLeadStatus { get; set; }
        public int ExpiryDays { get; set; }
        public string DefaultProductId { get; set; }
        public int LocExpiryInMonths { get; set; }
        public Dictionary<string, string> Dependencies { get; set; }
        public string Database { get; set; }
        public string ConnectionString { get; set; }
    }
}
