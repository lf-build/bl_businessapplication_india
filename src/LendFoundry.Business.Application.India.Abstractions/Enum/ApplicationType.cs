﻿using LendFoundry.Business.Applicant.India;

namespace LendFoundry.Business.Application.India
{
   public enum ApplicationType
    {
       New=1,
       Renewal=2
    }
}
