﻿
namespace LendFoundry.Business.Application.India
{
    public class ExternalReference : IExternalReference
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
