﻿using System.Collections.Generic;

namespace LendFoundry.Business.Application.India
{
    public interface IExternalReferences
    {
        string Name { get; set; }
        List<string> Value { get; set; }
    }
}
