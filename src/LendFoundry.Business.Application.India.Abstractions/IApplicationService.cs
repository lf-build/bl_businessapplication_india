﻿using System.Collections.Generic;
using System.Threading.Tasks;
using LendFoundry.InstantBankVerification;
using LendFoundry.ProductConfiguration;

namespace LendFoundry.Business.Application.India {
    public interface IApplicationService {
        Task<IApplicationResponse> Add (IApplicationRequest applicationRequest);

        Task<IApplicationResponse> AddMerchantApplication (IApplicationRequest request);

        Task<IApplication> GetByApplicationId (string applicationId);

        Task<IApplication> GetByApplicationNumber (string applicationId);

        Task LinkBankInformation (string applicationId, IBankInformation bankInformationRequest);

        Task LinkEmailInformation (string applicationId, IEmailAddress emailInformationRequest);

        Task SetSelfDeclaredInformation (string applicationId, ISelfDeclareInformation declareInformationRequest);

        Task<IApplicationResponse> UpdateApplication (string applicationId, IApplicationRequest applicationRequest);

        Task<IApplicationDetails> GetApplicationDetails (string applicationId);

        Task<IApplication> AddExternalSources (string applicationNumber, IExternalReferencesRequest externalReferences);

        Task<List<IExternalReferences>> GetExternalSources (string applicationNumber);
        Task<List<IApplication>> GetApplicationByApplicantId (string applicantId);
        Task<IApplication> SwitchProduct (string applicationNumber, string productId, string otherLenderNotes);
        Task UpdateDrawDownAmount (string applicationNumber, IUpdateDrawDownAmountRequest requestAmount);
        Task UpdateLoanAmount (string applicationNumber, IUpdateLoanAmountRequest requestLoanAmount);
        Task<IApplication> UpdateFields (string applicationId, Dictionary<string, object> applicationFields);
        Task SetPrimary (string applicationId, string ownerId);
        Task<dynamic> SaveProductAttribute (string ProductId, IProduct product);
        Task<string> InitiateWorkFlow (string productId, string applicationNumber, IProduct product, WorkFlowType WorkflowType = WorkFlowType.ApplicationApproval);
        Task<IApplicationResponse> AddRenewalApplication (string applicationNumber, IApplicationRequest applicationRequest);
        Task<List<IApplication>> GetRelatedApplications (string applicationId);

        Task<IBankAccount> AddManualDetails (string entityType, string entityId, IRequestManualBankLink request);
        Task<bool> AddAccountPreference (string entityType, string entityId, IAccountPreferenceRequest accountPreference);
        Task AddBankLink (string bankLinkId, string entityType, string entityId);
        Task<IAccountTypeResponse> GetAccountByType (string entityType, string entityId, IAccountTypeRequest request);
    }
}