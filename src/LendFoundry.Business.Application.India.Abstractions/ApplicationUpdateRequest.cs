﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.Business.Application.India
{
    public class ApplicationUpdateRequest : IApplicationUpdateRequest
    {
        public ApplicationUpdateRequest()
        {
        }

        public ApplicationUpdateRequest(IApplicationRequest request)
        {
            ContactFirstName = request.ContactFirstName;
            ContactLastName = request.ContactLastName;
            RequestedAmount = request.RequestedAmount;
            LoanTimeFrame = request.LoanTimeFrame;
            RequestedTermType = request.RequestedTermType;
            RequestedTermValue = request.RequestedTermValue;
            PurposeOfLoan = request.PurposeOfLoan;
            Source = request.Source;
            SelfDeclareInformation = request.SelfDeclareInformation;
            PrimaryPhone = request.PrimaryPhone;
            PrimaryEmail = request.PrimaryEmail;
            OtherPurposeDescription = request.OtherPurposeDescription;
            if (request.PrimaryApplicant != null && request.PrimaryApplicant.Owners != null && request.PrimaryApplicant.Owners.Count > 0)
            {
                Owners = new List<string>();
                foreach (var item in request.PrimaryApplicant.Owners)
                {
                    Owners.Add(item.OwnerId);
                }
            }
            LeadOwnerId = request.LeadOwnerId;
            PartnerId = request.PartnerId;
            PartnerUserId = request.PartnerUserId;
            ClientIpAddress = request.ClientIpAddress;
            OtherLenderNotes = request.OtherLenderNotes;
            
        }

        public string ContactFirstName { get; set; }
        public string ContactLastName { get; set; }
        public double RequestedAmount { get; set; }
        public string LoanTimeFrame { get; set; }
        public string RequestedTermType { get; set; }
        public double RequestedTermValue { get; set; }
        public string PurposeOfLoan { get; set; }

        [JsonConverter(typeof(InterfaceConverter<ISource, Source>))]
        public ISource Source { get; set; }

        public string ApplicationNumber { get; set; }

        public string PropertyType { get; set; }

        [JsonConverter(typeof(InterfaceConverter<ISelfDeclareInformation, SelfDeclareInformation>))]
        public ISelfDeclareInformation SelfDeclareInformation { get; set; }

        public TimeBucket ApplicationDate { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IPhoneNumber, PhoneNumber>))]
        public IPhoneNumber PrimaryPhone { get; set; }

        public string PrimaryFax { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IEmailAddress, EmailAddress>))]
        public IEmailAddress PrimaryEmail { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IAddress, Address>))]
        public IAddress PrimaryAddress { get; set; }

        public TimeBucket DateNeeded { get; set; }

        public string Signature { get; set; }

        public double MonthlyRent { get; set; }

        public double EMI { get; set; }
        public double MonthlyExpenses { get; set; }
        public double CreditCardBalances { get; set; }
        public string OtherPurposeDescription { get; set; }
        public List<string> Owners { get; set; }

        public string LeadOwnerId { get; set; }
        public string PartnerId { get; set; }
        public string PartnerUserId { get; set; }
        public string ClientIpAddress { get; set; }

        public string OtherLenderNotes { get; set; }

        public TimeBucket ProductApprovalDate { get; set; }
    }
    
}