﻿namespace LendFoundry.Business.Application.India {
    public enum AccountType {
        Savings = 1,
        Checking = 2
    }
}