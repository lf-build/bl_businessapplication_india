﻿using LendFoundry.Business.Applicant.India;

namespace LendFoundry.Business.Application.India.Events
{
    public class ApplicationCreated
    {
        public IApplication Application { get; set; }

        public IApplicant Applicant { get; set; }

        public string EntityId { get; set; }
        public string EntityType { get; set; }
    }
}
