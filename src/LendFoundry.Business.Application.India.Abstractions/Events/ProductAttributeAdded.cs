﻿

using LendFoundry.ProductConfiguration;

namespace LendFoundry.Business.Application.India.Events
{
    public class ProductAttributeAdded
    {
        public IProduct Product { get; set; }
        public string EntityId { get; set; }
    }
}
