﻿using LendFoundry.Business.Applicant.India;

namespace LendFoundry.Business.Application.India.Events
{
    public class ApplicationModified
    {
        public IApplication Application { get; set; }

        public IApplicant Applicant { get; set; }
    }
}
