﻿namespace LendFoundry.Business.Application.India
{
    public interface IPhoneNumber
    {
       
        string Phone { get; set; }
        string CountryCode { get; set; }
        PhoneType PhoneType { get; set; }      
        bool IsDefault { get; set; }
       
    }
}