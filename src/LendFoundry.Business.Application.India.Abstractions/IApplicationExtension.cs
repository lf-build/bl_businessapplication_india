﻿using LendFoundry.Business.Applicant.India;

namespace LendFoundry.Business.Application.India
{
    public interface IApplicationExtension
    {
        IApplication Application { get; set; }

        IApplicant Applicant { get; set; }

        string StatusWorkFlowId { get; set; }    
    }
}
