﻿namespace LendFoundry.Business.Application.India
{
    public class EmailAddress : IEmailAddress
    {
        public string Email { get; set; }
        public EmailType EmailType { get; set; }
      
        public bool IsDefault { get; set; }
       
    }
}
