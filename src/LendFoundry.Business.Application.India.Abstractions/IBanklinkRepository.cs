using System.Collections.Generic;
using System.Threading.Tasks;
using LendFoundry.Foundation.Persistence;
using LendFoundry.InstantBankVerification;

namespace LendFoundry.Business.Application.India.Persistence {
    public interface IBanklinkRepository : IRepository<IBankLink> {
        Task<string> GetAccessToken (string itemId);
        Task<List<IBankLink>> GetAllBankLinkData ();
        Task<IBankLink> GetBankLinkData (string itemId);
        Task<IBankLink> GetBankLinkDataByEntityId (string entityId);
        Task<IBankLink> GetBankLinkDataById (string entityId);
        Task<string> GetBankLinkId (string token);
        Task<IBankLink> GetBankLinkingData (string entityId, string id);
        Task<List<IBankLink>> GetBankLinksByEntityId (string entityId);
        Task<bool> IsBankLinked (string itemId, string token);
        Task<bool> IsBankLinkedByEntityId (string entityId);
        Task<bool> UpdateBankLink (string token);
    }
}