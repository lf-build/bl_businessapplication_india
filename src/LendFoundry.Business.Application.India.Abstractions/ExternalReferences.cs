﻿using System.Collections.Generic;

namespace LendFoundry.Business.Application.India
{
    public class ExternalReferences : IExternalReferences
    {
        public string Name { get; set; }
        public List<string> Value { get; set; }
    }
}
