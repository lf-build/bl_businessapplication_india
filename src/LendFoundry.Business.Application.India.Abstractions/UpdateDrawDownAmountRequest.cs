﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Business.Application.India
{
    public class UpdateDrawDownAmountRequest : IUpdateDrawDownAmountRequest
    {
        public double DrawDownAmount { get; set; }

    }
}
