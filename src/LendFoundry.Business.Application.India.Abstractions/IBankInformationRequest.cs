﻿namespace LendFoundry.Business.Application.India
{
    public interface IBankInformationRequest
    {
        string BankId { get; set; }
        string BankName { get; set; }
        string AccountNumber { get; set; }
        string AccountHolderName { get; set; }
        string RoutingNumber { get; set; }
        AccountType AccountType { get; set; }
    }
}
