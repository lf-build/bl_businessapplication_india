﻿namespace LendFoundry.Business.Application.India
{
    public enum AddressType
    {
      
        Home,
        Mailing,
        Business,
        Military,
        GST,
        Others,
        Residence,
        Work,
    }
}
