﻿namespace LendFoundry.Business.Application.India
{
    public interface ISelfDeclareInformation
    {
        double AnnualRevenue { get; set; }
        double SelfReportedAnnualRevenue { get; set; }
        double AverageBankBalance { get; set; }
        bool IsExistingBusinessLoan { get; set; }
    }
}
