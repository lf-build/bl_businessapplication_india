﻿namespace LendFoundry.Business.Application.India
{
    public enum EmailType
    {
        Personal,
        Work,
        Home,
        Others
    }
}
