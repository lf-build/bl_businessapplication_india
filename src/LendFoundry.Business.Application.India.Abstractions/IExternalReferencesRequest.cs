﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.Business.Application.India
{
    public interface IExternalReferencesRequest
    {
        [JsonConverter(typeof(InterfaceListConverter<IExternalReference, ExternalReference>))]
        List<IExternalReference> ExternalReferences { get; set; }
    }
}
