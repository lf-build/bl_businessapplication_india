﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.Business.Application.India
{
    public interface IApplicationUpdateRequest
    {
        string ContactFirstName { get; set; }
        string ContactLastName { get; set; }
        double RequestedAmount { get; set; }
        string LoanTimeFrame { get; set; }
        string RequestedTermType { get; set; }
        double RequestedTermValue { get; set; }
        string PurposeOfLoan { get; set; }

        ISource Source { get; set; }

        string ApplicationNumber { get; set; }

        string PropertyType { get; set; }

        ISelfDeclareInformation SelfDeclareInformation { get; set; }

        TimeBucket ApplicationDate { get; set; }

        IPhoneNumber PrimaryPhone { get; set; }

        string PrimaryFax { get; set; }

        IEmailAddress PrimaryEmail { get; set; }

        IAddress PrimaryAddress { get; set; }

        TimeBucket DateNeeded { get; set; }

        string Signature { get; set; }

        double MonthlyRent { get; set; }

        double EMI { get; set; }
        double MonthlyExpenses { get; set; }
        double CreditCardBalances { get; set; }
        string OtherPurposeDescription { get; set; }
        List<string> Owners { get; set; }

        string LeadOwnerId { get; set; }
        string PartnerId { get; set; }
        string PartnerUserId { get; set; }
        string ClientIpAddress { get; set; }

        string OtherLenderNotes { get; set; }
        TimeBucket ProductApprovalDate { get; set; }

    }
}