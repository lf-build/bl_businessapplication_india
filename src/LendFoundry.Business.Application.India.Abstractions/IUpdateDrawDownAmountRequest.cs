﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Business.Application.India
{
   public interface IUpdateDrawDownAmountRequest
    {
        double DrawDownAmount { get; set; }
    }
}
