﻿using LendFoundry.Business.Applicant.India;

namespace LendFoundry.Business.Application.India
{
    public class ApplicationExtension : IApplicationExtension
    {
        public IApplication Application { get; set; }
        public IApplicant Applicant { get; set; }
        public string StatusWorkFlowId { get; set; }
    }
}
