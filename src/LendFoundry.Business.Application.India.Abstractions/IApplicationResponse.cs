﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LendFoundry.Business.Application.India
{
    public interface IApplicationResponse
    {
        double RequestedAmount { get; set; }
        string RequestedTermType { get; set; }
        double RequestedTermValue { get; set; }
        string PurposeOfLoan { get; set; }

        [JsonConverter(typeof(InterfaceConverter<ISource, Source>))]
        ISource Source { get; set; }

        string ApplicantId { get; set; }
        string ApplicationNumber { get; set; }

        [JsonConverter(typeof(InterfaceConverter<ISelfDeclareInformation, SelfDeclareInformation>))]
        ISelfDeclareInformation SelfDeclareInformation { get; set; }

        TimeBucket ApplicationDate { get; set; }
        TimeBucket ExpiryDate { get; set; }

        string LeadOwnerId { get; set; }
        string PartnerId { get; set; }
        string PartnerUserId { get; set; }
        string ProductId { get; set; }
        string WorkFlowStatusId { get; set; }
        string ApplicationUrl { get; set; }
    }
}