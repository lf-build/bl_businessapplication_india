﻿namespace LendFoundry.Business.Application.India
{
    public class UpdateOtherLendersData : IUpdateOtherLendersData
    {
        public string OtherLenderNotes { get; set; }

    }
}
