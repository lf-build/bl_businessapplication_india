﻿using System.IO;
using LendFoundry.Business.Applicant.India.Client;
using LendFoundry.Business.Application.India.Persistence;
using LendFoundry.Configuration;
using LendFoundry.Configuration.Client;
using LendFoundry.DataAttributes.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup.Client;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.ServiceDependencyResolver;
using LendFoundry.Foundation.Services;
using LendFoundry.NumberGenerator.Client;
using LendFoundry.ProductConfiguration.Client;
using LendFoundry.Security.Encryption;
using LendFoundry.Security.Tokens;
using LendFoundry.StatusManagement.Client;
using LendFoundry.Tenant.Client;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.AspNetCore.Swagger;

namespace LendFoundry.Business.Application.India.Api {
    internal class Startup {
        public Startup (IHostingEnvironment env) { }

        public void ConfigureServices (IServiceCollection services) {
#if DOTNET2
            services.AddSwaggerGen (c => {
                c.SwaggerDoc ("docs", new Info {
                    Version = PlatformServices.Default.Application.ApplicationVersion,
                        Title = "DataAttributes"
                });
                c.AddSecurityDefinition ("apiKey", new ApiKeyScheme () {
                    Type = "apiKey",
                        Name = "Authorization",
                        Description = "For accessing the API a valid JWT token must be passed in all the queries in the 'Authorization' header. The syntax used in the 'Authorization' header should be Bearer xxxxx.yyyyyy.zzzz",
                        In = "header"
                });
                c.DescribeAllEnumsAsStrings ();
                c.IgnoreObsoleteProperties ();
                c.DescribeStringEnumsInCamelCase ();
                c.IgnoreObsoleteActions ();
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine (basePath, "LendFoundry.Business.Application.India.Api.xml");
                c.IncludeXmlComments (xmlPath);
            });
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor> ();

#else
            services.AddSwaggerDocumentation ();
#endif
            services.AddTenantTime ();
            services.AddTokenHandler ();
            services.AddHttpServiceLogging (Settings.ServiceName);
            services.AddConfigurationService<ApplicationConfiguration> (Settings.ServiceName);
            services.AddDependencyServiceUriResolver<ApplicationConfiguration> (Settings.ServiceName);
            services.AddEventHub (Settings.ServiceName);
            services.AddTenantService ();
            services.AddLookupService ();
            services.AddNumberGeneratorService ();
            services.AddApplicantService ();
            services.AddProductService ();
            services.AddEncryptionHandler ();
            //services.AddSingleton<IMongoConfiguration>(p => new MongoConfiguration(Settings.Mongo.ConnectionString, Settings.Mongo.Database));
            services.AddMongoConfiguration (Settings.ServiceName);
            services.AddStatusManagementService ();
            services.AddDataAttributes (); //("192.168.1.59", 5106);

            services.AddTransient<ApplicationConfiguration> (p => p.GetService<IConfigurationService<ApplicationConfiguration>> ().Get ());
            services.AddTransient<IApplicationRepository, ApplicationRepository> ();
            services.AddTransient<IApplicationService, ApplicationService> ();
            services.AddTransient<IApplicationConfiguration, ApplicationConfiguration> ();
            services.AddTransient<IBanklinkRepository, BankLinkRepository> ();
            services.AddTransient<IBankaccountRepository, BankAccountRepository> ();
            services.AddMvc ().AddLendFoundryJsonOptions ();
            services.AddCors ();
        }

        public void Configure (IApplicationBuilder app, IHostingEnvironment env) {
            app.UseHealthCheck ();
            app.UseCors (env);
#if DOTNET2
            app.UseSwagger ();

            app.UseSwaggerUI (c => {
                c.SwaggerEndpoint ("/swagger/docs/swagger.json", "Application Service");
            });
#else
            app.UseSwaggerDocumentation ();
#endif

            app.UseErrorHandling ();
            app.UseRequestLogging ();
            app.UseMvc ();
            app.UseHealthCheck ();
        }
    }
}