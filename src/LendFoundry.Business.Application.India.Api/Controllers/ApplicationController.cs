﻿using LendFoundry.Business.Applicant.India;
using LendFoundry.Foundation.Client;
using LendFoundry.Foundation.Services;
using LendFoundry.InstantBankVerification;
using LendFoundry.ProductConfiguration;
#if DOTNET2
using Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Mvc;
#endif
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.Business.Application.India.Api.Controllers {
    /// <summary>
    /// class ApplicationController
    /// </summary>
    [Route ("/")]
    public class ApplicationController : ExtendedController {
        #region Constructors
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="service"></param>

        public ApplicationController (IApplicationService service) {
            ApplicationService = service;
        }

        #endregion Constructors

        #region Private Properties

        private static NoContentResult NoContentResult { get; } = new NoContentResult ();
        private IApplicationService ApplicationService { get; }
        private new static readonly NoContentResult NoContent = new NoContentResult ();

        #endregion Private Properties

        #region Public Methods

        /// <summary>
        /// Add Application
        /// </summary>
        /// <param name="applicationRequest"></param>
        /// <returns></returns>
        [HttpPost]
#if DOTNET2
        [ProducesResponseType (typeof (IApplicationResponse), 200)]
#endif
        public async Task<IActionResult> Add ([FromBody] ApplicationRequest applicationRequest) {
            return await ExecuteAsync (async () => {
                try {
                    return Ok (await ApplicationService.Add (applicationRequest));
                } catch (UnableToCreateUserException exception) // once applicant application would be created
                {
                    return ErrorResult.BadRequest (exception.Message);
                } catch (AggregateException exception) {
                    return new ErrorResult (409, exception.Message);
                }
            });
        }

        /// <summary>
        /// GetApplication
        /// </summary>
        /// <param name="applicationNumber"></param>
        /// <returns></returns>
        [HttpGet ("{applicationNumber}")]
#if DOTNET2
        [ProducesResponseType (typeof (IApplication), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        [ProducesResponseType (typeof (ErrorResult), 404)]
#endif
        public async Task<IActionResult> Get (string applicationNumber) =>
            await ExecuteAsync (async () => Ok (await ApplicationService.GetByApplicationId (applicationNumber)));

        /// <summary>
        /// UpdateApplication
        /// </summary>
        /// <param name="applicationNumber"></param>
        /// <param name="applicationRequest"></param>
        /// <returns></returns>
        [HttpPut ("{applicationNumber}")]
#if DOTNET2
        [ProducesResponseType (typeof (IApplicationResponse), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
#endif
        public async Task<IActionResult> UpdateApplication (string applicationNumber, [FromBody] ApplicationRequest applicationRequest) =>
            await ExecuteAsync (async () => Ok (await ApplicationService.UpdateApplication (applicationNumber, applicationRequest)));

        /// <summary>
        /// LinkBankInformation
        /// </summary>
        /// <param name="applicationNumber"></param>
        /// <param name="bankInformationRequest"></param>
        /// <returns></returns>
        [HttpPut ("{applicationNumber}/bankinformation")]
        [ProducesResponseType (204)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        [ProducesResponseType (typeof (ErrorResult), 404)]
        public async Task<IActionResult> LinkBankInformation (string applicationNumber, [FromBody] BankInformation bankInformationRequest) =>
            await ExecuteAsync (async () => { await ApplicationService.LinkBankInformation (applicationNumber, bankInformationRequest); return NoContentResult; });

        /// <summary>
        /// LinkEmailInformation
        /// </summary>
        /// <param name="applicationNumber"></param>
        /// <param name="emailInfoRequest"></param>
        /// <returns></returns>
        [HttpPut ("{applicationNumber}/emailinformation")]
        [ProducesResponseType (204)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        [ProducesResponseType (typeof (ErrorResult), 404)]
        public async Task<IActionResult> LinkEmailInformation (string applicationNumber, [FromBody] EmailAddress emailInfoRequest) =>
            await ExecuteAsync (async () => { await ApplicationService.LinkEmailInformation (applicationNumber, emailInfoRequest); return NoContentResult; });

        /// <summary>
        ///  LinkSelfDeclaredExpense
        /// </summary>
        /// <param name="applicationNumber"></param>
        /// <param name="declareInformationRequest"></param>
        /// <returns></returns>
        [HttpPut ("{applicationNumber}/declaredexpense")]
        [ProducesResponseType (204)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        [ProducesResponseType (typeof (ErrorResult), 404)]
        public async Task<IActionResult> LinkSelfDeclaredExpense (string applicationNumber, [FromBody] SelfDeclareInformation declareInformationRequest) =>
            await ExecuteAsync (async () => { await ApplicationService.SetSelfDeclaredInformation (applicationNumber, declareInformationRequest); return NoContentResult; });

        /// <summary>
        /// GetDetails
        /// </summary>
        /// <param name="applicationNumber"></param>
        /// <returns></returns>
        [HttpGet ("{applicationNumber}/details")]
        [ProducesResponseType (typeof (IApplicationDetails), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        public async Task<IActionResult> GetDetails (string applicationNumber) =>
            await ExecuteAsync (async () => Ok (await ApplicationService.GetApplicationDetails (applicationNumber)));

        /// <summary>
        /// AddExternalSources
        /// </summary>
        /// <param name="applicationNumber"></param>
        /// <param name="externalReferencesRequest"></param>
        /// <returns></returns>
        [HttpPost ("{applicationNumber}/externalreferences")]
        [ProducesResponseType (typeof (IApplication), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        public async Task<IActionResult> AddExternalSources (string applicationNumber, [FromBody] ExternalReferencesRequest externalReferencesRequest) {
            return await ExecuteAsync (async () => {

                return Ok (await ApplicationService.AddExternalSources (applicationNumber, externalReferencesRequest));
            });
        }

        /// <summary>
        /// UpdateLoanAmount
        /// </summary>
        /// <param name="applicationNumber"></param>
        /// <param name="requestLoanAmount"></param>
        /// <returns></returns>
        [HttpPut ("{applicationNumber}/loanAmount")]
        [ProducesResponseType (204)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        [ProducesResponseType (typeof (ErrorResult), 404)]
        public async Task<IActionResult> UpdateLoanAmount (string applicationNumber, [FromBody] UpdateLoanAmountRequest requestLoanAmount) =>
            await ExecuteAsync (async () => { await ApplicationService.UpdateLoanAmount (applicationNumber, requestLoanAmount); return NoContentResult; });

        /// <summary>
        /// UpdateDrawDownAmount
        /// </summary>
        /// <param name="applicationNumber"></param>
        /// <param name="updateDrawDownAmountRequest"></param>
        /// <returns></returns>
        [HttpPut ("{applicationNumber}/drawdownAmount")]
        [ProducesResponseType (204)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        [ProducesResponseType (typeof (ErrorResult), 404)]
        public async Task<IActionResult> UpdateDrawDownAmount (string applicationNumber, [FromBody] UpdateDrawDownAmountRequest updateDrawDownAmountRequest) =>
            await ExecuteAsync (async () => { await ApplicationService.UpdateDrawDownAmount (applicationNumber, updateDrawDownAmountRequest); return NoContentResult; });

        /// <summary>
        /// GetExternalSources 
        /// </summary>
        /// <param name="applicationNumber"></param>
        /// <returns></returns>
        [HttpGet ("{applicationNumber}/externalreferences")]
        [ProducesResponseType (typeof (List<IExternalReferences>), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        [ProducesResponseType (typeof (ErrorResult), 404)]
        public async Task<IActionResult> GetExternalSources (string applicationNumber) {
            return await ExecuteAsync (async () => {

                return Ok (await ApplicationService.GetExternalSources (applicationNumber));
            });
        }
#if DOTNET2
        /// <summary>
        /// GetApplicationByApplicantId
        /// </summary>
        /// <param name="applicantId"></param>
        /// <returns></returns>
        [HttpGet ("application/by/{applicantId}")]
        [ProducesResponseType (typeof (List<IApplication>), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
#endif
        public async Task<IActionResult> GetApplicationByApplicantId (string applicantId) =>
            await ExecuteAsync (async () => Ok (await ApplicationService.GetApplicationByApplicantId (applicantId)));

        /// <summary>
        /// SwitchProduct
        /// </summary>
        /// <param name="applicationNumber"></param>
        /// <param name="productId"></param>
        /// <param name="OtherLendersData"></param>
        /// <returns></returns>
        [HttpPost ("{applicationNumber}/switchProduct/{productId}")]
        [ProducesResponseType (typeof (IApplication), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        [ProducesResponseType (typeof (ErrorResult), 404)]
        public async Task<IActionResult> SwitchProduct (string applicationNumber, string productId, [FromBody] UpdateOtherLendersData OtherLendersData) // [FromBody]Dictionary<string, object> fields
        {
            return await ExecuteAsync (async () => {

                return Ok (await ApplicationService.SwitchProduct (applicationNumber, productId, OtherLendersData == null ? string.Empty : OtherLendersData.OtherLenderNotes));
            });
        }

        /// <summary>
        ///  UpdateFields
        /// </summary>
        /// <param name="applicationId"></param>
        /// <param name="fields"></param>
        /// <returns></returns>
        [HttpPut ("{applicationId}/update/fields")]
        [ProducesResponseType (typeof (IApplication), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        public async Task<IActionResult> UpdateFields (string applicationId, [FromBody] Dictionary<string, object> fields) {
            return await ExecuteAsync (async () => Ok (await ApplicationService.UpdateFields (applicationId, fields)));
        }

        /// <summary>
        ///  SetPrimary
        /// </summary>
        /// <param name="applicationId"></param>
        /// <param name="ownerId"></param>
        /// <returns></returns>
        [HttpPut ("{applicationId}/set/{ownerId}")]
        [ProducesResponseType (204)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        public async Task<IActionResult> SetPrimary (string applicationId, string ownerId) =>
            await ExecuteAsync (async () => { await ApplicationService.SetPrimary (applicationId, ownerId); return NoContentResult; });

        /// <summary>
        ///  SaveProductAttribute
        /// </summary>
        /// <param name="ProductId"></param>
        /// <param name="product"></param>
        /// <returns></returns>
        [HttpPost ("saveProductAttribute/{ProductId}")]
        public async Task<IActionResult> SaveProductAttribute (string ProductId, Product product) {
            return await ExecuteAsync (async () => {
                try {
                    return Ok (await ApplicationService.SaveProductAttribute (ProductId, product));
                } catch (UnableToCreateUserException exception) {
                    return ErrorResult.BadRequest (exception.Message);
                } catch (AggregateException exception) {
                    return new ErrorResult (409, exception.Message);
                }
            });
        }

        /// <summary>
        /// Multi Product InitiateWorkFlow
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="applicationNumber"></param>
        /// <param name="product"></param>
        /// <param name="WorkflowType"></param>
        /// <returns></returns>
        [HttpPost ("initiateWorkFlow/{productId}/{applicationNumber}/{WorkflowType?}")]
        public async Task<IActionResult> InitiateWorkFlow (string productId, string applicationNumber, [FromBody] Product product, WorkFlowType WorkflowType) {
            return await ExecuteAsync (async () => {
                try {
                    return Ok (await ApplicationService.InitiateWorkFlow (productId, applicationNumber, product, WorkFlowType.ApplicationApproval));
                } catch (UnableToCreateUserException exception) {
                    return ErrorResult.BadRequest (exception.Message);
                } catch (AggregateException exception) {
                    return new ErrorResult (409, exception.Message);
                }
            });
        }

        /// <summary>
        /// AddRenewalApplication
        /// </summary>
        /// <param name="applicationNumber"></param>
        /// <param name="applicationRequest"></param>
        /// <returns></returns>
        [HttpPost ("add/renewal/{applicationNumber}")]
#if DOTNET2
        [ProducesResponseType (typeof (IApplicationResponse), 200)]
#endif
        public async Task<IActionResult> AddRenewalApplication (string applicationNumber, [FromBody] ApplicationRequest applicationRequest) {
            return await ExecuteAsync (async () => {
                try {
                    return Ok (await ApplicationService.AddRenewalApplication (applicationNumber, applicationRequest));
                } catch (UnableToCreateUserException exception) {
                    return ErrorResult.BadRequest (exception.Message);
                } catch (AggregateException exception) {
                    return new ErrorResult (409, exception.Message);
                }
            });
        }

        /// <summary>
        /// GetRelatedApplications
        /// </summary>
        /// <param name="applicationNumber"></param>
        /// <returns></returns>
        [HttpGet ("related/applications/{applicationNumber}")]
#if DOTNET2
        [ProducesResponseType (typeof (IApplication), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        [ProducesResponseType (typeof (ErrorResult), 404)]
#endif
        public async Task<IActionResult> GetRelatedApplications (string applicationNumber) =>
            await ExecuteAsync (async () => Ok (await ApplicationService.GetRelatedApplications (applicationNumber)));

        /// <summary>
        /// AddAccountPreference
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="accountPreference"></param>
        /// <returns></returns>
        [HttpPost ("{entitytype}/{entityId}/add/account/preference")]
        [ProducesResponseType (typeof (bool), 200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        [ProducesResponseType (typeof (ErrorResult), 404)]
        public Task<IActionResult> AddAccountPreference (string entityType, string entityId, [FromBody] AccountPreferenceRequest accountPreference) {
            return ExecuteAsync (async () => {
                try {
                    var result = await ApplicationService.AddAccountPreference (entityType, entityId, accountPreference);
                    return Ok (result);
                } catch (Exception ex) {
                    Logger.Error (ex.Message, ex, new { entityType, entityId, accountPreference });
                    return ErrorResult.BadRequest (ex.Message);
                }
            });
        }

        /// <summary>
        /// ManualUploadDetails
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <returns></returns>  
        [HttpPost ("{entityType}/{entityId}/manual/details")]
        [ProducesResponseType (200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        [ProducesResponseType (typeof (ErrorResult), 404)]
        public async Task<IActionResult> ManualUploadDetails (string entityType, string entityId, [FromBody] RequestManualBankLink request) {
            try {
                var result = await ApplicationService.AddManualDetails (entityType, entityId, request);
                return Ok (result);
            } catch (Exception ex) {
                Logger.Error (ex.Message, ex, new { entityType, entityId, request });
                return ErrorResult.BadRequest (ex.Message);
            }
        }

        /// <summary>
        /// GetAccountByType
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost ("{entitytype}/{entityId}/accountTypeSet")]
        [ProducesResponseType (typeof (IAccountTypeResponse), 200)]
        [ProducesResponseType (typeof (NoContentResult), 204)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        [ProducesResponseType (typeof (ErrorResult), 404)]
        public Task<IActionResult> GetAccountByType (string entityType, string entityId, [FromBody] AccountTypeRequest request) {
            return ExecuteAsync (async () => {
                try {
                    var result = await ApplicationService.GetAccountByType (entityType, entityId, request);
                    if (result == null) {
                        return NoContent;
                    }
                    return Ok (result);
                } catch (Exception ex) {
                    Logger.Error (ex.Message, ex, new { entityType, entityId, request });
                    return ErrorResult.BadRequest (ex.Message);
                }
            });
        }

        /// <summary>
        /// AddBankLinks
        /// </summary>
        /// <param name="bankLinkId"></param>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <returns></returns>
        [HttpGet ("{bankLinkId}/{entityType}/{entityId}/addBankLinks")]
        [ProducesResponseType (200)]
        [ProducesResponseType (typeof (ErrorResult), 400)]
        [ProducesResponseType (typeof (ErrorResult), 404)]
        public Task<IActionResult> AddBankLinks (string bankLinkId, string entityType, string entityId) {
            return ExecuteAsync (async () => {
                try {
                    await ApplicationService.AddBankLink (bankLinkId, entityType, entityId);
                    return Ok ();
                } catch (Exception ex) {
                    Logger.Error (ex.Message, ex, new { bankLinkId, entityType, entityId });
                    return ErrorResult.BadRequest (ex.Message);
                }
            });
        }

        #endregion Public Methods
    }
}