﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.Foundation.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.Services;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using MongoDB.Driver.Linq;

namespace LendFoundry.Business.Application.India.Persistence {
    public class ApplicationRepository : MongoRepository<IApplication, Application>, IApplicationRepository {
        #region Constructor

        static ApplicationRepository () {
            BsonClassMap.RegisterClassMap<Application> (map => {
                map.AutoMap ();
                var type = typeof (Application);
                map.SetDiscriminator ($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass (true);
            });
            BsonClassMap.RegisterClassMap<TimeBucket> (map => {
                map.AutoMap ();
                map.MapMember (m => m.Time).SetSerializer (new DateTimeOffsetSerializer (BsonType.Document));

                var type = typeof (TimeBucket);
                map.SetDiscriminator ($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass (false);
            });
            BsonClassMap.RegisterClassMap<Applicant.India.Applicant> (map => {
                map.AutoMap ();
                map.MapProperty (p => p.Addresses).SetIgnoreIfDefault (true);
                //map.MapProperty (p => p.PhoneNumbers).SetIgnoreIfDefault (true);
                var type = typeof (Applicant.India.Applicant);
                map.SetDiscriminator ($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            BsonClassMap.RegisterClassMap<Address> (map => {
                map.AutoMap ();
                map.MapProperty (p => p.AddressType).SetSerializer (new EnumSerializer<AddressType> (BsonType.String));
                var type = typeof (Address);
                map.SetDiscriminator ($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            BsonClassMap.RegisterClassMap<PhoneNumber> (map => {
                map.AutoMap ();
                map.MapProperty (p => p.PhoneType).SetSerializer (new EnumSerializer<PhoneType> (BsonType.String));
                var type = typeof (PhoneNumber);
                map.SetDiscriminator ($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            BsonClassMap.RegisterClassMap<EmailAddress> (map => {
                map.AutoMap ();
                map.MapProperty (p => p.EmailType).SetSerializer (new EnumSerializer<EmailType> (BsonType.String));
                var type = typeof (EmailAddress);
                map.SetDiscriminator ($"{type.FullName}, {type.Assembly.GetName().Name}");
            });
            BsonClassMap.RegisterClassMap<ExternalReferences> (map => {
                map.AutoMap ();
                var type = typeof (ExternalReferences);
                map.SetDiscriminator ($"{type.FullName}, {type.Assembly.GetName().Name}");
            });
            BsonClassMap.RegisterClassMap<Source> (map => {
                map.AutoMap ();
                var type = typeof (Source);
                map.SetDiscriminator ($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            BsonClassMap.RegisterClassMap<SelfDeclareInformation> (map => {
                map.AutoMap ();
                var type = typeof (SelfDeclareInformation);
                map.SetDiscriminator ($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            BsonClassMap.RegisterClassMap<BankInformation> (map => {
                map.AutoMap ();
                map.MapProperty (p => p.AccountType).SetSerializer (new EnumSerializer<AccountType> (BsonType.String));
            
                var type = typeof (BankInformation);
                map.SetDiscriminator ($"{type.FullName}, {type.Assembly.GetName().Name}");
            });
        }

        public ApplicationRepository (LendFoundry.Tenant.Client.ITenantService tenantService, IMongoConfiguration configuration) : base (tenantService, configuration, "application-india") {
            CreateIndexIfNotExists ("applications_applicationId", Builders<IApplication>.IndexKeys.Ascending (i => i.ApplicationNumber));
        }

        #endregion Constructor

        #region PublicMethods

        #region Public Methods

        public async Task<IApplication> GetByApplicationId (string applicationId) {
            return await Task.Run (() => Query.Where (application => application.TenantId == TenantService.Current.Id && application.ApplicationNumber == applicationId).FirstOrDefault ());
        }

        public async Task<string> GetApplicantId (string applicationId) {
            var application = await Task.Run (() => Query.Where (app => app.ApplicationNumber == applicationId).FirstOrDefault ());
            return application.ApplicantId;
        }

        public async Task<IApplication> LinkBankInformation (string applicationNumber, IBankInformation bankInformation) {
            var application = await GetByApplicationId (applicationNumber);
            if (application == null)
                throw new NotFoundException ($"Application {applicationNumber} not found");

            await Collection.UpdateOneAsync (Builders<IApplication>.Filter.Where (a => a.TenantId == TenantService.Current.Id &&
                    a.ApplicationNumber == applicationNumber),
                Builders<IApplication>.Update.Set (a => a.LinkedBankInformation, bankInformation));

            return application;
        }

        public async Task<IApplication> LinkEmailInformation (string applicationNumber, IEmailAddress emailInformation) {
            var application = await GetByApplicationId (applicationNumber);

            if (application == null)
                throw new NotFoundException ($"Application {applicationNumber} not found");
            application.LinkedEmailAddress = emailInformation;

            await Collection.UpdateOneAsync (Builders<IApplication>.Filter.Where (a => a.TenantId == TenantService.Current.Id &&
                    a.ApplicationNumber == applicationNumber),
                Builders<IApplication>.Update.Set (a => a.LinkedEmailAddress, emailInformation));
            return application;
        }

        public async Task<IApplication> UpdateApplication (string applicationId, IApplicationUpdateRequest applicationRequest) {
            var application = await GetByApplicationId (applicationId);

            await Collection.UpdateOneAsync (Builders<IApplication>.Filter.Where (a => a.TenantId == application.TenantId &&
                    a.ApplicationNumber == applicationId),
                Builders<IApplication>.Update.Set (a => a.RequestedAmount, applicationRequest.RequestedAmount)
                .Set (a => a.RequestedTermType, applicationRequest.RequestedTermType)
                .Set (a => a.RequestedTermValue, applicationRequest.RequestedTermValue)
                .Set (a => a.PurposeOfLoan, applicationRequest.PurposeOfLoan)
                .Set (a => a.ContactFirstName, applicationRequest.ContactFirstName)
                .Set (a => a.ContactLastName, applicationRequest.ContactLastName)
                .Set (a => a.SelfDeclareInformation, applicationRequest.SelfDeclareInformation)
                .Set (a => a.LoanTimeFrame, applicationRequest.LoanTimeFrame)
                .Set (a => a.OtherPurposeDescription, applicationRequest.OtherPurposeDescription)
                .Set (a => a.Source, applicationRequest.Source)
                .Set (a => a.PropertyType, applicationRequest.PropertyType)
                .Set (a => a.Owners, applicationRequest.Owners)
                .Set (a => a.PrimaryPhone, applicationRequest.PrimaryPhone)
                .Set (a => a.PrimaryEmail, applicationRequest.PrimaryEmail)
                //.Set (a => a.PrimaryAddress, applicationRequest.PrimaryAddress)
                .Set (a => a.PrimaryFax, applicationRequest.PrimaryFax)
                //.Set (a => a.DateNeeded, applicationRequest.DateNeeded)
                .Set (a => a.ClientIpAddress, applicationRequest.ClientIpAddress)
                //.Set (a => a.BusinessPhone, applicationRequest.BusinessPhone)
                .Set (a => a.OtherLenderNotes, applicationRequest.OtherLenderNotes)
                .Set (a => a.ProductApprovalDate, applicationRequest.ProductApprovalDate)
                .Set (a => a.ApplicationDate, applicationRequest.ApplicationDate)
            );

            application = await GetByApplicationId (applicationId);
            return application;
        }

        public async Task<IApplication> SetSelfDeclaredInformation (string applicationNumber, ISelfDeclareInformation declareExpenseInformation) {
            var application = await GetByApplicationId (applicationNumber);

            if (application == null)
                throw new NotFoundException ($"Application {applicationNumber} not found");

            application.SelfDeclareInformation = declareExpenseInformation;

            await Collection.UpdateOneAsync (Builders<IApplication>.Filter.Where (a => a.TenantId == TenantService.Current.Id &&
                    a.ApplicationNumber == applicationNumber),
                Builders<IApplication>.Update.Set (a => a.SelfDeclareInformation, declareExpenseInformation));

            return application;
        }

        public async Task<List<IApplication>> GetApplicationByApplicantId (string applicantId) {
            return await Query.Where (a => a.TenantId == TenantService.Current.Id && a.ApplicantId == applicantId).ToListAsync ();
        }
        public async Task UpdateFields (string applicationNumber, IDictionary<string, object> fields) {
            var filter = Builders<IApplication>.Filter.Where (a => a.TenantId == TenantService.Current.Id &&
                a.ApplicationNumber == applicationNumber);
            var firstField = fields.FirstOrDefault ();
            fields.Remove (firstField.Key);
            var update = Builders<IApplication>.Update.Set (firstField.Key as string, firstField.Value);
            if (fields.Count > 0) {
                update = update.SetDictionary (fields);
            }
            await Collection.UpdateOneAsync (filter, update);
        }


        public async Task<List<IApplication>> GetRelatedApplications(string applicationId) {
          var applicationDetails=await GetByApplicationId(applicationId);
          var result= await Task.Run(() => Query.Where(application =>application.OriginalParentApplicationNumber == applicationDetails.OriginalParentApplicationNumber && application.ApplicationNumber != applicationId).ToList());
          return result;
        }
        #endregion Public Methods

        #endregion PublicMethods
    }
}