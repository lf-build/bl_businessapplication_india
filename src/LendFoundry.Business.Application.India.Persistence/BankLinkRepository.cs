using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.Foundation.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.Services;
using LendFoundry.InstantBankVerification;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using MongoDB.Driver.Linq;

namespace LendFoundry.Business.Application.India.Persistence {

    /// <summary>
    /// 
    /// </summary>
    public class BankLinkRepository : MongoRepository<IBankLink, BankLink>, IBanklinkRepository {
        #region Constructor
        static BankLinkRepository () {
            BsonClassMap.RegisterClassMap<BankLink> (map => {
                map.AutoMap ();
                var type = typeof (BankLink);
                map.SetDiscriminator ($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass (true);
            });

            BsonClassMap.RegisterClassMap<Link> (map => {
                map.AutoMap ();
                var type = typeof (Link);
                map.SetDiscriminator ($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

        }
        /// <summary>
        /// BankLinkRepository
        /// </summary>
        /// <param name="tenantService"></param>
        /// <param name="configuration"></param>
        public BankLinkRepository (LendFoundry.Tenant.Client.ITenantService tenantService, IMongoConfiguration configuration):
            base (tenantService, configuration, "banklink-india") {
                CreateIndexIfNotExists ("tenant_id", Builders<IBankLink>.IndexKeys
                    .Ascending (x => x.TenantId), false);

                CreateIndexIfNotExists ("provider_bank_link_id", Builders<IBankLink>.IndexKeys
                    .Ascending (x => x.ProviderBankLinkId), false);

                CreateIndexIfNotExists ("provider_token", Builders<IBankLink>.IndexKeys
                    .Ascending (x => x.ProviderToken), false);

                CreateIndexIfNotExists ("entity_id", Builders<IBankLink>.IndexKeys
                    .Ascending (x => x.TenantId)
                    .Ascending (x => x.EntityId)
                    .Ascending (x => x.ProviderBankLinkId), true);
            }
        #endregion Constructor

        /// <summary>
        /// GetBankLinkId
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<string> GetBankLinkId (string token) {
            var bankLinkData = (await Collection.FindAsync (x => x.ProviderToken == token && x.IsDeleted == false)).FirstOrDefault ();

            if (bankLinkData != null)
                return bankLinkData.Id;
            else
                return string.Empty;
        }

        /// <summary>
        /// GetBankLinkIdByEntityId
        /// </summary>
        /// <param name="entityId"></param>
        /// <returns></returns>

        public async Task<IBankLink> GetBankLinkDataByEntityId (string entityId) {
            int plaid = (int) Provider.Plaid;
            return (await Collection.FindAsync (x => x.EntityId == entityId && x.ProviderId == plaid && x.IsDeleted == false)).FirstOrDefault ();
        }

        /// <summary>
        /// GetBankLinksByEntityId
        /// </summary>
        /// <param name="entityId"></param>
        /// <returns></returns>
        public async Task<List<IBankLink>> GetBankLinksByEntityId (string entityId) {
            return (await Collection.FindAsync (x => x.EntityId == entityId && x.IsDeleted == false && x.TenantId == TenantService.Current.Id)).ToList ();
        }

        /// <summary>
        /// UpdateBankLink
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<bool> UpdateBankLink (string token) {

            var bankLinkData = (await Collection.FindAsync (x => x.ProviderToken == token && x.IsDeleted == false)).FirstOrDefault ();

            if (bankLinkData != null)
                return (await Collection.UpdateOneAsync (
                    Builders<IBankLink>.Filter
                    .Where (objAccount => objAccount.TenantId == TenantService.Current.Id &&
                        objAccount.ProviderToken == token),
                    Builders<IBankLink>.Update
                    .Set (objAccount => objAccount.IsDeleted, true)
                    .Set (objAccount => objAccount.UpdatedDate, new TimeBucket ())
                )).IsAcknowledged;

            return false;
        }
        /// <summary>
        /// UpdateBankLink
        /// </summary>
        /// <param name="entityId"></param>
        /// <param name="ProviderId"></param>
        /// <returns></returns>

        public async Task<bool> UpdateBankLink (string entityId, int ProviderId) {

            var bankLinkData = (await Collection.FindAsync (x => x.EntityId == entityId && x.ProviderId == ProviderId)).FirstOrDefault ();

            if (bankLinkData != null)
                return (await Collection.UpdateOneAsync (
                    Builders<IBankLink>.Filter
                    .Where (objAccount => objAccount.TenantId == TenantService.Current.Id &&
                        objAccount.EntityId == entityId && objAccount.ProviderId == ProviderId),
                    Builders<IBankLink>.Update
                    .Set (objAccount => objAccount.IsDeleted, true)
                    .Set (objAccount => objAccount.UpdatedDate, new TimeBucket ())
                )).IsAcknowledged;

            return false;
        }

        /// <summary>
        /// GetAccessToken
        /// </summary>
        /// <param name="itemId"></param>
        /// <returns></returns>
        public async Task<string> GetAccessToken (string itemId) {
            var bankLinkData = (await Collection.FindAsync (x => x.ProviderBankLinkId == itemId && x.IsDeleted == false && x.TenantId == TenantService.Current.Id)).FirstOrDefault ();

            if (bankLinkData != null)
                return bankLinkData.ProviderToken;
            else
                return string.Empty;
        }

        /// <summary>
        /// GetBankLinkData
        /// </summary>
        /// <param name="itemId"></param>
        /// <returns></returns>
        public async Task<IBankLink> GetBankLinkData (string itemId) {
            var bankLinkData = (await Collection.FindAsync (x => x.ProviderBankLinkId == itemId && x.IsDeleted == false && x.TenantId == TenantService.Current.Id)).FirstOrDefault ();

            return bankLinkData;
        }

        /// <summary>
        /// IsBankLinked
        /// </summary>
        /// <param name="itemId"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<bool> IsBankLinked (string itemId, string token) {
            var bankLinkData = (await Collection.FindAsync (x => x.ProviderBankLinkId == itemId && x.ProviderToken == token && x.IsDeleted == false && x.TenantId == TenantService.Current.Id)).FirstOrDefault ();

            if (bankLinkData != null)
                return true;
            else
                return false;
        }

        /// <summary>
        /// IsBankLinkedByEntityId
        /// </summary>
        /// <param name="entityId">entityId</param>
        /// <returns></returns>
        public async Task<bool> IsBankLinkedByEntityId (string entityId) {
            var bankLinkData = (await Collection.FindAsync (x => x.EntityId == entityId && x.IsDeleted == false && x.TenantId == TenantService.Current.Id)).FirstOrDefault ();

            if (bankLinkData != null)
                return true;
            else
                return false;
        }

        /// <summary>
        /// GetBankLinkData
        /// </summary>
        /// <param name="entityId"></param>
        /// <returns></returns>
        public async Task<IBankLink> GetBankLinkDataById (string entityId) {
            var bankLinkData = (await Collection.FindAsync (x => x.EntityId == entityId && x.IsDeleted == false && x.TenantId == TenantService.Current.Id)).FirstOrDefault ();

            return bankLinkData;
        }

        /// <summary>
        /// GetBankLinkingData
        /// </summary>
        /// <param name="entityId"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<IBankLink> GetBankLinkingData (string entityId, string id) {
            var bankLinkData = (await Collection.FindAsync (x => x.EntityId == entityId && x.Id == id && x.IsDeleted == false && x.TenantId == TenantService.Current.Id)).FirstOrDefault ();

            return bankLinkData;
        }
        /// <summary>
        /// GetAllBankLinkData
        /// </summary>
        /// <returns></returns>

        public async Task<List<IBankLink>> GetAllBankLinkData () {
            int plaid = (int) Provider.Plaid;
            return await Query.Where (x => x.ProviderId == plaid && x.IsDeleted == false && x.TenantId == TenantService.Current.Id).ToListAsync ();
        }
    }

}